package com.felixcool98.widgets.imageeditor.canvas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Cursor.SystemCursor;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.felixcool98.widgets.PixmapUtils;
import com.felixcool98.widgets.Widgets;
import com.kotcrab.vis.ui.building.utilities.Alignment;

public class DrawableCanvas extends SimpleCanvas {
	private EditorAction currentAction = new EditorAction.Point();
	
	private Cursor drawCursor;
	
	private Color drawColor;
	
	
	public DrawableCanvas() {
		this(Widgets.getSkin().get(DrawableCanvasStyle.class));
	}
	public DrawableCanvas(Skin skin) {
		this(skin.get(DrawableCanvasStyle.class));
	}
	public DrawableCanvas(DrawableCanvasStyle style) {
		super(style);
		
		setStyle(style);
		
		setupListeners();
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	public void setStyle(DrawableCanvasStyle style) {
		if(style == null)
			return;
		
		setStyle((SimpleCanvasStyle) style);
		
		if(style.hasDrawCursor()) {
			drawCursor = Gdx.graphics.newCursor(style.drawCursor, style.drawCursorHotspotX, style.drawCursorHotspotY);
		}
	}
	
	public void setEditorAction(EditorAction action) {
		this.currentAction = action;
	}
	
	public void setDrawColor(Color color) {
		drawColor = color;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public EditorAction getEditorAction() {
		return currentAction;
	}
	
	public Color getDrawColor() {
		return drawColor;
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	public boolean hasDrawCursor() {
		return drawCursor != null;
	}
	
	
	//======================================================================
	// init
	//======================================================================
	private void setupListeners() {
		addListener(new InputListener() {
			@Override
			public boolean mouseMoved(InputEvent event, float x, float y) {
				if(hasDrawCursor())
					Gdx.graphics.setCursor(drawCursor);
				
				return super.mouseMoved(event, x, y);
			}
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if(hasDrawCursor())
					Gdx.graphics.setCursor(drawCursor);
			}
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if(hasDrawCursor())
					Gdx.graphics.setSystemCursor(SystemCursor.Arrow);
			}
		});
		
		setupImageListeners();
	}
	private void setupImageListeners() {
		getImage().addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				float imageX = toPixelX(x);
				float imageY = toPixelY(y);
				
				if(currentAction != null) {
					 currentAction.touchDown(DrawableCanvas.this, imageX, imageY, button);
				}
				
				return super.touchDown(event, imageX, imageY, pointer, button);
			}
			@Override
			public void clicked(InputEvent event, float x, float y) {
				float imageX = toPixelX(x);
				float imageY = toPixelY(y);
				
				if(currentAction != null) {
					currentAction.clicked(DrawableCanvas.this, imageX, imageY);
				}
			}
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if(pointer != -1)
					clearToolLayer();
			}
		});
		getImage().addListener(new ActorGestureListener() {
			@Override
			public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
				float imageX = toPixelX(x);
				float imageY = toPixelY(y);
				
				if(imageX < 0 || imageY < 0 || imageX >= getImage().getPixmapWidth() || imageY >= getImage().getImageHeight())
					return;
				
				if(currentAction != null) {
					currentAction.pan(DrawableCanvas.this, imageX, imageY, deltaX, deltaY);
				}
			}
		});
		getImage().addListener(new DragListener() {
			
			@Override
			public void drag(InputEvent event, float x, float y, int pointer) {
				float imageX = toPixelX(x);
				float imageY = toPixelY(y);
				
				if(imageX < 0 || imageY < 0 || imageX >= getImage().getPixmapWidth() || imageY >= getImage().getImageHeight())
					return;
				
				if(currentAction != null) {
					currentAction.drag(DrawableCanvas.this, imageX, imageY);
				}
			}
			@Override
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				float imageX = toPixelX(x);
				float imageY = toPixelY(y);
				
				if(imageX < 0 || imageY < 0 || imageX >= getImage().getPixmapWidth() || imageY >= getImage().getImageHeight())
					return;
				
				if(currentAction != null) {
					currentAction.dragStop(DrawableCanvas.this, imageX, imageY);
				}
			}
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if(pointer != -1)
					clearToolLayer();
			}
		});
	}
	
	
	//======================================================================
	// resize
	//======================================================================
	/**
	 * 
	 * @param width the new width of the image
	 * @param height the new height of the image
	 * @param alignment where to place the old image in the resized version
	 * @param scale the scale to apply should be a power of to
	 */
	public void resizeImage(int width, int height, Alignment alignment, float scale) {
		if(width <= 0 || height <= 0)
			return;
		
		if(scale <= 0)
			scale = 1;
		
		if(alignment == null)
			alignment = Alignment.CENTER;
		if(scale < 1)
			alignment = Alignment.BOTTOM_LEFT;
		
		Pixmap pixmap = new Pixmap(width, height, getImage().getPixmapFormat());
		
		int scaledWidth = (int) (getImage().getPixmapWidth()*scale);
		int scaledHeight = (int) (getImage().getPixmapHeight()*scale);
		
		int x = width/2-scaledWidth/2;
		int y = height/2-scaledHeight/2;
		
		if(alignment.isAlignedWithLeft())
			x = 0;
		else if(alignment.isAlignedWithRight())
			x = width-scaledWidth;
		
		if(alignment.isAlignedWithTop())
			y = height-scaledHeight;
		else if(alignment.isAlignedWithBottom())
			y = 0;
		
		if(scale == 1) {
			pixmap.drawPixmap(getImage().getPixmap(), x, y);
		}else if(scale > 1) {
			pixmap.drawPixmap(PixmapUtils.upscale(getImage().getPixmap(), (int) scale), x, y);
		}else if(scale < 1) {
			for(int i = 0; i < scaledWidth; i++) {
				for(int j = 0; j < scaledHeight; j++) {
					int pixel = 0;
					int values = 0;
					
					for(float px = 0; px < 1/scale; px++) {
						for(float py = 0; py < 1/scale; py++) {
							int value = getImage().pixmapGetPixel((int) (i*scale+px), (int) (j*scale+py));
							
							if(value == 0)
								continue;
							
							pixel += value;
							
							values++;
						}
					}
					
					if(values == 0)
						continue;
					
					pixel = pixel / values;
					
					pixmap.drawPixel(i, j, pixel);
				}
			}
		}
		
		setImage(pixmap);
	}
	
	
	//======================================================================
	// drawing
	//======================================================================
	public void drawPoint(float x, float y, float size) {
		drawPoint((int)x, (int)y, Math.round(size));
	}
	public void drawPoint(int x, int y, int size) {
		if(size > 1)
			drawRectFilled(x-(int)((float)size/2f), y-(int)((float)size/2f), size, size);
		else {
			getImage().pixmapSetColor(getDrawColor());
			getImage().pixmapDrawPixel(x, y);
		}
	}
	
	public void drawLine(float x1, float y1, float x2, float y2) {
		drawLine((int)x1, (int)y1, (int)x2, (int)y2);
	}
	public void drawLine(int x1, int y1, int x2, int y2) {
		getImage().pixmapSetColor(getDrawColor());
		
		getImage().pixmapDrawLine(x1, y2, x2, y2);
		
	}
	
	public void drawFillAll() {
		getImage().pixmapSetColor(getDrawColor());
		
		getImage().pixmapFill();
	}
	
	public void drawFill(float x, float y) {
		drawFill((int)x, (int)y);
	}
	public void drawFill(int x, int y) {
		PixmapUtils.fillPixmap(getImage().getPixmap(), x, y, getDrawColor());
	}
	
	public void drawRectFilled(int x, int y, int width, int height) {
		getImage().pixmapSetColor(getDrawColor());
		
		for(int i = 0; i < width; i++) {
			getImage().pixmapDrawLine(x+i, y, x+i, y+height-1);
		}
	}
	
	public void drawRectOutline(int x, int y, int width, int height) {
		getImage().pixmapSetColor(getDrawColor());
		
		getImage().pixmapDrawRectangle(x, y, width, height);
	}
	
	
	//======================================================================
	// Style
	//======================================================================
	public static class DrawableCanvasStyle extends SimpleCanvasStyle {
		public int drawCursorHotspotX;
		public int drawCursorHotspotY;
		
		public Pixmap drawCursor;
		
		
		public boolean hasDrawCursor() {
			return drawCursor != null;
		}
	}
}
