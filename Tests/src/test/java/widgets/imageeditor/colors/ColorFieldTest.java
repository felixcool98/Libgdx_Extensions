package widgets.imageeditor.colors;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.widgets.imageeditor.colors.ColorField;

public class ColorFieldTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("Color Field Test");
		config.setWindowSizeLimits(1920, 1080, 1920, 1080);
		
		new Lwjgl3Application(new ColorFieldTest(), config);
	}
	
	
	@Override
	public void create() {
		ColorField field = new ColorField();
		field.setPosition(20, 20);
		field.setSize(30, 30);
		field.setColor(Color.BROWN);
		
		getStage().addActor(field);
	}
}
