package com.felixcool98.logging.logger;

import com.felixcool98.logging.logtype.LogType;
import com.felixcool98.utility.filter.Filter;

public interface Logger {
	public void log(LogType type, String message);
	public default void log(String[] tags, int level, String message) {
		log(new LogType(level, tags), message);
	}
	public default void log(int level, String message) {
		log(new LogType(level), message);
	}
	public default void log(Exception e) {
		log(LogType.EXCEPTION, e.getMessage());
	}
	public default void log(String text) {
		log(LogType.MESSAGE, text);
	}
	
	public void setFilter(Filter<LogType> filter);
	
	
	
	public abstract class AbstractLogger implements Logger{
		private Filter<LogType> filter;
		
		
		@Override
		public void setFilter(Filter<LogType> filter) {
			this.filter = filter;
		}
		
		
		protected abstract void logThis(LogType type, String message);
		
		
		@Override
		public void log(LogType type, String message) {
			if(filter != null && !filter.valid(type))
				return;
			
			logThis(type, message);
		}
	}
}
