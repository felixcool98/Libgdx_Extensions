package com.felixcool98.widgets.imageeditor.canvas;

import com.badlogic.gdx.Input.Buttons;

public interface EditorAction {
	public void pan(DrawableCanvas canvas, float x, float y, float deltaX, float deltaY);
	public void clicked(DrawableCanvas canvas, float x, float y);
	public boolean touchDown(DrawableCanvas canvas, float x, float y, int button);
	public void drag(DrawableCanvas canvas, float x, float y);
	public void dragStop(DrawableCanvas canvas, float x, float y);
	
	
	public static class EditorActionAdapter implements EditorAction {
		@Override
		public void pan(DrawableCanvas canvas, float x, float y, float deltaX, float deltaY) {
		}

		@Override
		public void clicked(DrawableCanvas canvas, float x, float y) {
		}

		@Override
		public boolean touchDown(DrawableCanvas canvas, float x, float y, int button) {
			return false;
		}

		@Override
		public void drag(DrawableCanvas canvas, float x, float y) {
		}

		@Override
		public void dragStop(DrawableCanvas canvas, float x, float y) {
		}
	}
	
	
	public static class Point extends EditorActionAdapter {
		@Override
		public void clicked(DrawableCanvas canvas, float x, float y) {
			canvas.drawPoint(x, y, 1);
		}

		@Override
		public boolean touchDown(DrawableCanvas canvas, float x, float y, int button) {
			if(button == Buttons.LEFT)
				canvas.drawPoint(x, y, 1);
			
			return false;
		}
		
		@Override
		public void drag(DrawableCanvas canvas, float x, float y) {
			canvas.drawPoint(x, y, 1);
		}
	}
	
	
	public static class Line extends EditorActionAdapter {
		private int lineX, lineY;
		
		@Override
		public void pan(DrawableCanvas canvas, float x, float y, float deltaX, float deltaY) {
			canvas.clearToolLayer();
			canvas.drawLineOnToolLayer((int) lineX, (int) lineY, (int) x, (int) y);
		}

		@Override
		public void clicked(DrawableCanvas canvas, float x, float y) {
			canvas.drawLine(x, y, lineX, lineY);
			canvas.clearToolLayer();
		}

		@Override
		public boolean touchDown(DrawableCanvas canvas, float x, float y, int button) {
			this.lineX = (int) x;
			this.lineY = (int) y;
			
			return false;
		}
		
		@Override
		public void drag(DrawableCanvas canvas, float x, float y) {
			canvas.clearToolLayer();
			canvas.drawLineOnToolLayer((int) lineX, (int) lineY, (int) x, (int) y);
		}
	}
	
	
	public static class Fill extends EditorActionAdapter {
		@Override
		public void clicked(DrawableCanvas canvas, float x, float y) {
			canvas.drawFill(x, y);
		}
	}
	
	
	public static class Move extends EditorActionAdapter {
		@Override
		public void pan(DrawableCanvas canvas, float x, float y, float deltaX, float deltaY) {
			canvas.moveImageBy(deltaX, deltaY);
		}
	}
	
	
	public static class ZoomIn extends EditorActionAdapter {
		@Override
		public void clicked(DrawableCanvas canvas, float x, float y) {
			canvas.multiplyImageScale(1.3f);
		}
	}
	
	
	public static class ZoomOut extends EditorActionAdapter {
		@Override
		public void clicked(DrawableCanvas canvas, float x, float y) {
			canvas.divideImageScale(1.3f);
		}
	}
	
	
	public static class MaxZoomOut extends EditorActionAdapter {
		@Override
		public void clicked(DrawableCanvas canvas, float x, float y) {
			canvas.setImageScaleToInitalScale();
		}
	}
}
