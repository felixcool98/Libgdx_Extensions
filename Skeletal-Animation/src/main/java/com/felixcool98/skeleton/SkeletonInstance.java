package com.felixcool98.skeleton;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class SkeletonInstance implements ISkeletonInstance<Bone> {
	private Bone firstBone;
	
	
	
	//======================================================================
	// setter
	//======================================================================
	@Override
	public Bone setFirstBone(String name) {
		return setFirstBone(new Bone(name));
	}
	@Override
	public Bone setFirstBone(Bone bone) {
		firstBone = bone;
		
		return bone;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	@Override
	public Bone getBone(String name) {
		return firstBone.getChild(name);
	}
	
	
	//======================================================================
	// rendering
	//======================================================================
	public void renderDebugLines(ShapeRenderer renderer, float parentX, float parentY, float parentAngle) {
		firstBone.renderDebugLines(renderer, parentX, parentY, parentAngle);
	}
	
	
	@Override
	public SkeletonInstance clone() {
		SkeletonInstance clone = new SkeletonInstance();
		
		List<Bone> bones = firstBone.getAllBones();
		
		while(!bones.isEmpty()) {
			List<Bone> remove = new LinkedList<>();
			
			for(Bone bone : bones) {
				if(bone.getParent() == null) {
					Bone newBone = clone.setFirstBone(bone.getName());
					newBone.setAll(bone);
				
					remove.add(bone);
				}else {
					Bone parent = clone.getBone(bone.getParent().getName());
					
					if(parent == null)
						continue;
					
					Bone newBone = parent.addChild(bone.getName());
					newBone.setAll(bone);
					
					remove.add(bone);
				}
			}
			
			bones.removeAll(remove);
			
			if(remove.isEmpty())
				break;
		}
		
		return clone;
	}
}
