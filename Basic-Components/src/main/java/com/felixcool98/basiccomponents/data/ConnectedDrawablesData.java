package com.felixcool98.basiccomponents.data;

import com.felixcool98.basiccomponents.components.ConnectedDrawables;
import com.felixcool98.data.Data;
import com.felixcool98.utility.values.AdvancedDirection;

public class ConnectedDrawablesData implements Data {
	private AdvancedDirection surrounding;
	
	
	public AdvancedDirection getSurrounding() {
		return surrounding;
	}
	
	
	public void setSurrounding(AdvancedDirection surrounding) {
		this.surrounding = surrounding;
	}
	
	
	@Override
	public Class<ConnectedDrawables> getDataClass() {
		return ConnectedDrawables.class;
	}

}
