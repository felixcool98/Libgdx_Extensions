package com.felixcool98.basiccomponents.components;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.felixcool98.resources.Image;
import com.felixcool98.resources.Resources;

public class ComponentDrawable {
	private Drawable drawable;
	
	
	public ComponentDrawable(String path) {
		setDrawable(path);
	}
	public ComponentDrawable(FileHandle handle) {
		setDrawable(handle);
	}
	public ComponentDrawable(Image image) {
		setDrawable(image);
	}
	public ComponentDrawable(Texture texture) {
		setDrawable(texture);
	}
	public ComponentDrawable(TextureRegion region) {
		setDrawable(region);
	}
	public ComponentDrawable(Drawable drawable) {
		setDrawable(drawable);
	}
	
	
	public Drawable getDrawable() {
		return drawable;
	}
	
	
	public void setDrawable(String path) {
		setDrawable(Resources.getImage(path));
	}
	public void setDrawable(FileHandle handle) {
		setDrawable(Resources.getImage(handle));
	}
	public void setDrawable(Image image) {
		setDrawable(image.getTexture());
	}
	public void setDrawable(Texture texture) {
		setDrawable(new TextureRegion(texture));
	}
	public void setDrawable(TextureRegion region) {
		setDrawable(new TextureRegionDrawable(region));
	}
	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
	
	
	public void draw(Batch batch, float x, float y, float width, float height) {
		if(drawable == null)
			return;
		
		drawable.draw(batch, x, y, width, height);
	}
}
