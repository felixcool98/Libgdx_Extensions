package com.felixcool98.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.felixcool98.widgets.imageeditor.canvas.DrawableCanvas;
import com.felixcool98.widgets.imageeditor.colors.ColorPalette;

public class Widgets {
	private static Skin skin;
	
	
	public static void load() {
		if(skin != null)
			return;
		
		skin = new Skin();
		skin.add("default", new ColorPalette.ColorPaletteStyle());
		
		skin.add("default", new DrawableCanvas.SimpleCanvasStyle());{
			DrawableCanvas.SimpleCanvasStyle style = skin.get(DrawableCanvas.SimpleCanvasStyle.class);
			
			style.imageBackground = new TextureRegionDrawable(new TextureRegion(new Texture(PixmapUtils.createBackgroundPattern(16, Color.WHITE, Color.LIGHT_GRAY))));
		}
		skin.add("default", new DrawableCanvas.DrawableCanvasStyle());{
			DrawableCanvas.DrawableCanvasStyle style = skin.get(DrawableCanvas.DrawableCanvasStyle.class);
			
			style.imageBackground = new TextureRegionDrawable(new TextureRegion(new Texture(PixmapUtils.createBackgroundPattern(16, Color.WHITE, Color.LIGHT_GRAY))));
			
			style.drawCursor = new Pixmap(Gdx.files.classpath("com/felixcool98/widgets/res/drawCursor.png"));
			style.drawCursorHotspotX = 8;
			style.drawCursorHotspotY = 8;
		}
		NinePatch patch = new NinePatch(new Texture(Gdx.files.classpath("com/felixcool98/widgets/res/SaveDisplayBackground.png")), 16, 16, 16, 16);
		skin.add("SaveDisplayBackground", new NinePatchDrawable(patch));
	}
	
	
	public static Skin getSkin() {
		load();
		
		return skin;
	}
}
