package com.felixcool98.gui.voicechat;

import com.esotericsoftware.kryo.Kryo;

public class VoiceChatKryo {
	public static void prepareKry(Kryo kryo) {
		kryo.register(short[].class);
	}
}
