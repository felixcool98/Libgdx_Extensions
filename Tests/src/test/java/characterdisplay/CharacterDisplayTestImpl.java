package characterdisplay;

import com.felixcool98.dialogs.Character;
import com.felixcool98.dialogs.gui.CharacterDisplay;
import com.felixcool98.game.ImprovedGameState;

public class CharacterDisplayTestImpl extends ImprovedGameState {
	@Override
	public void create() {
		Character character = new Character("name");
		character.useDefaultImage();
		CharacterDisplay display = new CharacterDisplay();
		display.setWidth(64);
		display.setImageHeight(64);
		display.rebuild();
		
		display.setCharacter(character, "default");
		display.setPosition(100, 100);
		
		getStage().addActor(display);
	}
}
