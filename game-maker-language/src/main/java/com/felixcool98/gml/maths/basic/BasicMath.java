package com.felixcool98.gml.maths.basic;

import com.felixcool98.gml.maths.Adder;
import com.felixcool98.gml.maths.Dividor;
import com.felixcool98.gml.maths.IntegerMod;
import com.felixcool98.gml.maths.MathFunction;
import com.felixcool98.gml.maths.Multiplier;
import com.felixcool98.gml.maths.Subtractor;

public class BasicMath {
	public static boolean isBasic(String text) {
		if(text.equals("+"))
			return true;
		else if(text.equals("*"))
			return true;
		else if(text.equals("-"))
			return true;
		else if(text.equals("/"))
			return true;
		else if(text.equals("mod"))
			return true;
		
		return false;
	}
	public static MathFunction createBasic(MathFunction f1, String sign, MathFunction f2) {
		if(sign.equals("+"))
			return new Adder(f1, f2);
		else if(sign.equals("*"))
			return new Multiplier(f1, f2);
		else if(sign.equals("-"))
			return new Subtractor(f1, f2);
		else if(sign.equals("/"))
			return new Dividor(f1, f2);
		else if(sign.equals("mod"))
			return new IntegerMod(f1, f2);
		
		System.err.println("couldn't read sign : " + sign.toString());
		
		return null;
	}
}
