package com.felixcool98.gui.chat;

import java.util.LinkedList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;

public class ChatClient extends com.felixcool98.net.Client {
	private List<MessageReceiver> receivers = new LinkedList<>();
	
	private String name;
	
	
	public ChatClient(String name) {
		super();
		
		this.name = name;
		
		addListener(new ConnectionListener() {
			@Override
			public void disconnected() {
				
			}
			
			@Override
			public void connected() {
				sendMessage(new Message.ConnectionMessage(ChatClient.this.name, "connected"));
			}
		});
	}
	
	
	
	@Override
	public void received(Connection connection, Object object) {
		if(object instanceof Message) {
			messageSent((Message) object);
		}
	}
	@Override
	public void registerClasses(Kryo kryo) {
		ChatKryo.prepareKry(kryo);
	}
	
	
	public void sendMessage(Message message) {
		getClient().sendTCP(message);
	}
	
	
	
	public void addMessageReceiver(MessageReceiver receiver) {
		receivers.add(receiver);
	}
	
	
	private void messageSent(Message message) {
		if(message == null)
			return;
		
		for(MessageReceiver receiver : receivers) {
			receiver.messageReceived(message);
		}
	}
	
	
	public boolean isConnected() {
		return getClient().isConnected();
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	public static interface MessageReceiver {
		public void messageReceived(Message message);
	}
}
