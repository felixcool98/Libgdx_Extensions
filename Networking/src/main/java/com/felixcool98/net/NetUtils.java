package com.felixcool98.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class NetUtils {
	public static String getIPv6() {
		try {
			URL whatismyip = new URL("https://v6.ident.me/");
	        BufferedReader in = null;
	        try {
	            in = new BufferedReader(new InputStreamReader(
	                    whatismyip.openStream()));
	            String ip = in.readLine();
	            return ip;
	        } finally {
	            if (in != null) {
	                try {
	                    in.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
