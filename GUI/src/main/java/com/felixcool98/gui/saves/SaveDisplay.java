package com.felixcool98.gui.saves;

import java.util.Calendar;
import java.util.Date;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.utility.values.Time;
import com.felixcool98.widgets.html.HTMLTableBuilder;
import com.kotcrab.vis.ui.widget.VisImage;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;

public class SaveDisplay extends Table {
	private Table texts;
	private VisLabel name;
	private Table times;
	private VisLabel saveTime;
	private VisLabel timePlayed;
	
	private Table imageDescription;
	private VisImage image;
	private VisScrollPane descriptionPane;
	private Table descriptionTable;
	
	
	public SaveDisplay(SaveDisplayStyle style) {
		super();
		
		init(style);
		
		pad(2);
	}
	
	
	private void init(SaveDisplayStyle style) {
		texts = new Table();{
			name = new VisLabel();{
				name.setWidth(style.width/2f);
			}texts.add(name).width(style.width/2f).align(Align.topLeft);
			
			times = new Table();{
				saveTime = new VisLabel();{
					saveTime.setWidth(style.width/2f);
					saveTime.setAlignment(Align.right);
				}times.add(saveTime).width(style.width/2f).align(Align.right);
				
				times.row();
				
				timePlayed = new VisLabel();{
					timePlayed.setWidth(style.width/2f);
					timePlayed.setAlignment(Align.right);
				}times.add(timePlayed).width(style.width/2f).align(Align.right);
			}texts.add(times).width(style.width/2f);
		}add(texts).width(style.width).pad(2);
		
		row();
		
		imageDescription = new Table();{
			image = new VisImage();{
				image.setSize(style.imageSize, style.imageSize);
			}imageDescription.add(image).size(style.imageSize);
			
			descriptionTable = new Table();
			
			descriptionPane = new VisScrollPane(descriptionTable);{
				descriptionPane.setFadeScrollBars(false);
				descriptionPane.setOverscroll(false, false);
			}imageDescription.add(descriptionPane).width(style.width - style.imageSize).height(style.imageSize);
		}add(imageDescription).width(style.width).height(style.imageSize).pad(2);
	}
	
	
	public void setSave(String name, Drawable image, Date saveTime, Time time, String description) {
		if(name != null)
			this.name.setText(name);
		if(saveTime != null)
			this.saveTime.setText(getString(saveTime));
		if(time != null)
			this.timePlayed.setText(time.toString());
		
		if(image != null)
			this.image.setDrawable(image);
		
		descriptionTable.clear();
		
		if(description != null) {
			Table html = new HTMLTableBuilder(description).build();
			
			html.align(Align.topLeft);
			
			descriptionTable.add(html).align(Align.topLeft);
		}
		
		pack();
	}
	
	
	public static String getString(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);date.toString();
		
		StringBuilder sb = new StringBuilder(28);

		sprintf0d(sb, calendar.get(Calendar.HOUR_OF_DAY), 2).append(':');   // HH
        sprintf0d(sb, calendar.get(Calendar.MINUTE), 2).append(':'); // mm
        sprintf0d(sb, calendar.get(Calendar.SECOND), 2).append(' '); // ss
		
		sprintf0d(sb, calendar.get(Calendar.DAY_OF_MONTH), 2).append('.'); // dd
		sprintf0d(sb, calendar.get(Calendar.MONTH), 2).append('.'); // mm
		sb.append(' ').append(calendar.get(Calendar.YEAR));  // yyyy
		
		return sb.toString();
	}
	/**
	 * copy of CalenderUtils.sprintf0d(StringBuilder sb, int value, int width)
     * Mimics sprintf(buf, "%0*d", decaimal, width).
     */
    public static final StringBuilder sprintf0d(StringBuilder sb, int value, int width) {
        long d = value;
        if (d < 0) {
            sb.append('-');
            d = -d;
            --width;
        }
        int n = 10;
        for (int i = 2; i < width; i++) {
            n *= 10;
        }
        for (int i = 1; i < width && d < n; i++) {
            sb.append('0');
            n /= 10;
        }
        sb.append(d);
        return sb;
    }
	
	
	public static class SaveDisplayStyle {
		public float imageSize = 64;
		
		public float width = 128; 
	}
}
