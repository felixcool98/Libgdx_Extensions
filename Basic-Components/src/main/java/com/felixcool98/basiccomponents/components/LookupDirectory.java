package com.felixcool98.basiccomponents.components;

import com.badlogic.gdx.files.FileHandle;

public class LookupDirectory {
	private FileHandle lookUpDir;
	
	
	public LookupDirectory(FileHandle lookUpDir) {
		this.lookUpDir = lookUpDir;
	}
	
	
	public FileHandle getLookUpDir() {
		return lookUpDir;
	}
	public FileHandle child(String path) {
		return lookUpDir.child(path);
	}
}
