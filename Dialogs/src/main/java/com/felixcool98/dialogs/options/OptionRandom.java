package com.felixcool98.dialogs.options;

import com.felixcool98.utility.random.Random;

public abstract class OptionRandom extends Option {
	private float chance;
	
	
	public OptionRandom(String text, float chance) {
		super(text);
		
		hideIfOptionCantBeClicked(true);
		
		this.chance = chance;
	}
	
	
	@Override
	public boolean canBeClicked() {
		return Random.INSTANCE.chance(chance);
	}
}
