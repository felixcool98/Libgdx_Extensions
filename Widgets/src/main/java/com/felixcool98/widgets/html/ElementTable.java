package com.felixcool98.widgets.html;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class ElementTable extends Table {
	@Override
	protected void drawBackground(Batch batch, float parentAlpha, float x, float y) {
		if (getBackground() == null) 
			return;
		
		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
		getBackground().draw(batch, x+getPadLeft(), y+getPadBottom(), getWidth()-getPadX(), getHeight()-getPadY());
	}
}
