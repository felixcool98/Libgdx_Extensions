package com.felixcool98.game;

import com.badlogic.gdx.ApplicationListener;

public abstract class GameState implements ApplicationListener {
	private GameStateManager stateManager;
	
	
	@Override
	public abstract void create();
	@Override
	public abstract void pause();
	@Override
	public abstract void resume();
	@Override
	public abstract void dispose();
	@Override
	public abstract void render();
	@Override
	public abstract void resize(int width, int height);
	
	
	//==============================================
	// convenience methods
	//==============================================
	
	public void enter(String state) {
		stateManager.enter(state);
	}
	public void enter(GameState state) {
		stateManager.enter(state);
	}
	
	
	void setManager(GameStateManager manager) {
		this.stateManager = manager;
	}
}
