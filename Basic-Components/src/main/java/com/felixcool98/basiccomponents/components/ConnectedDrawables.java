package com.felixcool98.basiccomponents.components;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.felixcool98.basiccomponents.BasicComponentDataManager;
import com.felixcool98.basiccomponents.data.ConnectedDrawablesData;
import com.felixcool98.components.HasData;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.ComponentManager;
import com.felixcool98.utility.filter.Filter;
import com.felixcool98.utility.values.AdvancedDirection;

public class ConnectedDrawables implements HasData.CanCreateData<ConnectedDrawablesData> {
	private Map<Integer, Drawable> drawables = new HashMap<>();
	private Filter<BasicComponentDataManager> filter;
	
	
	public void setFilter(Filter<BasicComponentDataManager> filter) {
		this.filter = filter;
	}
	
	public void put(Texture texture) {
		put(new TextureRegion(texture));
	}
	public void put(TextureRegion region) {
		int tileWidth = region.getRegionWidth() / 4;
		int tileHeight = region.getRegionHeight() / 4;
		
		TextureRegion[][] regions = region.split(tileWidth, tileHeight);
		
		for(int y = 0; y < 4; y++) {
			for(int x = 0; x < 4; x++) {
				AdvancedDirection direction = new AdvancedDirection();
				
				if(x == 0)
					direction.right();
				if(x == 1)
					direction.left().right();
				if(x == 2)
					direction.left();
				
				if(y == 0)
					direction.bot();
				if(y == 1)
					direction.top().bot();
				if(y == 2)
					direction.top();
				
				put(regions[y][x], direction);
			}
		}
	}
	
	public void put(Texture texture, AdvancedDirection direction) {
		put(new TextureRegion(texture), direction);
	}
	public void put(TextureRegion textureRegion, AdvancedDirection direction) {
		put(new TextureRegionDrawable(textureRegion), direction);
	}
	public void put(Drawable drawable, AdvancedDirection direction) {
		drawables.put(direction.get(), drawable);
	}
	
	
	public Drawable getDrawable(AdvancedDirection direction) {
		Drawable drawable = drawables.get(direction.get());
		
		if(drawable == null)
			drawable = drawables.get(direction.toDirection().get());
		
		return drawable;
	}
	public Filter<BasicComponentDataManager> getFilter(){
		return filter;
	}
	
	
	public void draw(Batch batch, float x, float y, float size) {
		float i = 0;
		float j = 0;
		
		Drawable drawable = null;
		
		//row 1
		
		drawable = getDrawable(new AdvancedDirection().bot().right());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().bot().right().left());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().bot().left());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().bot());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size); 
		i = 0; j -= size+2;
		
		//row 2
		
		drawable = getDrawable(new AdvancedDirection().top().right().bot());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().bot().right().left().top());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().bot().left().top());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().bot().top());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size); 
		i = 0; j -= size+2;
		
		//row 3
		
		drawable = getDrawable(new AdvancedDirection().top().right());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().top().right().left());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().top().left());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().top());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size); 
		i = 0; j -= size+2;
		
		//row 4
		
		drawable = getDrawable(new AdvancedDirection().right());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().right().left());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection().left());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size);
		i+= size+2;
		
		drawable = getDrawable(new AdvancedDirection());
		if(drawable != null)
			drawable.draw(batch, x + i, y + j, size, size); 
	}
	
	
	@Override
	public ConnectedDrawablesData create(ComponentManager manager, CreationData data) {
		return new ConnectedDrawablesData();
	}
}
