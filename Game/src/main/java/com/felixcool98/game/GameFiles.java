package com.felixcool98.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Files.FileType;
import com.felixcool98.resources.Directory;
import com.felixcool98.resources.Resources;

public class GameFiles {
	private Directory gameFolder;
	
	private Directory saveFolder;
	
	private Directory configFolder;
	
	
	public GameFiles(GameFilesConfig config) {
		gameFolder = Resources.getDirectory(Gdx.files.getFileHandle(config.gameFolder, config.gameFolderType));
		saveFolder = gameFolder.getDirectory(config.saveFolder);
		configFolder = gameFolder.getDirectory(config.configFolder);
	}
	
	
	public Directory getGameFolder() {
		return gameFolder;
	}
	public Directory getSaveFolder() {
		return saveFolder;
	}
	public Directory getConfigFolder() {
		return configFolder;
	}
	
	
	public static class GameFilesConfig {
		public String gameFolder = "";
		public FileType gameFolderType = FileType.Local;
		
		public String saveFolder = "/saves";
		public String configFolder = "/config";
	}
}
