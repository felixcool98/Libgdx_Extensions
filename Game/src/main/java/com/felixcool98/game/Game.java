package com.felixcool98.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.felixcool98.dialogs.res.DefaultValues;
import com.felixcool98.events.EventManager;
import com.felixcool98.game.GameFiles.GameFilesConfig;
import com.felixcool98.resources.Config;
import com.felixcool98.resources.Directory;
import com.felixcool98.resources.Image;
import com.felixcool98.resources.Music;
import com.felixcool98.resources.Resources;
import com.felixcool98.resources.SML;
import com.felixcool98.resources.Sound;
import com.felixcool98.resources.XML;
import com.felixcool98.utility.threads.ThreadUtils;
import com.kotcrab.vis.ui.VisUI;

public abstract class Game extends ApplicationAdapter {
	public static Game GAME;
	
	
	private GameFiles.GameFilesConfig gameFilesConfig;
	
	private GameFiles gameFiles;
	
	private EventManager eventManager = new EventManager();
	
	private GameStateManager stateManager = new GameStateManager();
	
	
	public Game() {
		this(new GameFiles.GameFilesConfig());
	}
	public Game(GameFiles.GameFilesConfig gameFilesConfig) {
		this.gameFilesConfig = gameFilesConfig;
		
		registerResourceHandlers();
		
		GAME = this;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public GameFiles getGameFiles() {
		return gameFiles;
	}
	
	public EventManager getEventManager() {
		return eventManager;
	}
	
	
	//======================================================================
	// create
	//======================================================================
	@Override
	public void create() {
		try {
			DefaultValues.load();
		} catch (NoClassDefFoundError e) {
			System.err.println("Couldn't find Dialogs skipping loading of defaults");
		}
		
		if(!VisUI.isLoaded())
			VisUI.load();
		
		gameFiles = new GameFiles(gameFilesConfig);
		
		ApplicationListener current = registerGameStates(stateManager);
		
		stateManager.enter(current);
	}
	protected void registerResourceHandlers() {
		Resources.registerResourceHandler(new Image.ImageResouceHandler());
		Resources.registerResourceHandler(new Directory.DirectoryResourceHandler());
		Resources.registerResourceHandler(new XML.XMLResourceHandler());
		Resources.registerResourceHandler(new Config.ConfigResourceHandler());
		Resources.registerResourceHandler(new Sound.SoundResourceHandler());
		Resources.registerResourceHandler(new Music.MusicResourceHandler());
		Resources.registerResourceHandler(new SML.SMLResourceHandler());
	}
	protected abstract ApplicationListener registerGameStates(GameStateManager manager);
	
	
	//======================================================================
	// dispose
	//======================================================================
	@Override
	public void dispose() {
		stateManager.dispose();
		Resources.disposeResources();
	}
	
	
	//======================================================================
	// rendering
	//======================================================================
	/**
	 * should not be overridden to keep full usability of this class<br>
	 * if you want to render everything on your own use the {@link Game#renderGame()} method and set autorender to false
	 */
	@Deprecated
	@Override
	public void render() {
		stateManager.render();
		
		ThreadUtils.update();
	}
	
	
	@Override
	public void resize(int width, int height) {
		stateManager.resize(width, height);
	}
	
	
	/**
	 * wraps an ApplicationListener in a game<br>
	 * can be convenient for initializing {@link Resources} and accessing {@link GameFiles}
	 * 
	 * @param state
	 * @param gameFilesConfig
	 * @return the created game
	 */
	public static ApplicationListener wrap(ApplicationListener state, GameFilesConfig gameFilesConfig) {
		return new Game(gameFilesConfig) {
			@Override
			protected ApplicationListener registerGameStates(GameStateManager manager) {
				return state;
			}
		};
	}
	/**
	 * wraps an ApplicationListener in a game<br>
	 * can be convenient for initializing {@link Resources} and accessing {@link GameFiles}
	 * 
	 * @param state
	 * @return the created game
	 */
	public static ApplicationListener wrap(ApplicationListener state) {
		return new Game() {
			@Override
			protected ApplicationListener registerGameStates(GameStateManager manager) {
				return state;
			}
		};
	}
}
