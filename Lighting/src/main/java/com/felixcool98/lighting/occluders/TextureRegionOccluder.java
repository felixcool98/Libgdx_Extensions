package com.felixcool98.lighting.occluders;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class TextureRegionOccluder implements Occluder {
	private TextureRegion region;
	
	private float x, y;
	
	private float width, height;
	
	
	public TextureRegionOccluder(TextureRegion region, float x, float y, float width, float height) {
		this.region = region;
		
		this.x = x;
		this.y = y;
		
		this.width = width;
		this.height = height;
	}
	
	
	@Override
	public void drawOcculuder(Batch batch, ShapeRenderer shape) {
		if(region == null)
			return;
		
		batch.draw(region, x, y, width, height);
	}

}
