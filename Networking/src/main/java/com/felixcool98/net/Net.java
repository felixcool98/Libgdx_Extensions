package com.felixcool98.net;

import com.esotericsoftware.kryo.Kryo;

public interface Net {
	public void registerClasses(Kryo kryo);
}
