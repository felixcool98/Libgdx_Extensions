package gui.saves;

import java.util.Date;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gui.saves.SaveDisplay;
import com.felixcool98.gui.saves.SaveDisplay.SaveDisplayStyle;
import com.felixcool98.utility.values.Time;
import com.felixcool98.widgets.Widgets;

public class SaveDisplayTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("SaveDisplayTest");
		config.setWindowSizeLimits(1000, 800, 1000, 800);
		
		new Lwjgl3Application(new SaveDisplayTest(), config);
	}
	
	
	@Override
	public void create() {
		SaveDisplayStyle style = new SaveDisplayStyle();{
			style.width = 512;
		}
		
		SaveDisplay display = new SaveDisplay(style);
		
		Time time = new Time();
		time.addHours(100);
		
		display.setSave("save 1", null, new Date(), time, "hello world<br> damn<br>long text<br> scrolling");
		display.setPosition(200, 100, Align.bottomLeft);
		display.setBackground(Widgets.getSkin().get("SaveDisplayBackground", NinePatchDrawable.class));
		getStage().addActor(display);
		
		getStage().setDebugAll(true);
	}
}
