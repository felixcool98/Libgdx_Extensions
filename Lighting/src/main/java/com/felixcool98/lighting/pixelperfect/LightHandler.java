package com.felixcool98.lighting.pixelperfect;

import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.lighting.lightsources.Light;
import com.felixcool98.lighting.occluders.Occluder;

public interface LightHandler<T extends Light> {
	public void render(T light, Batch batch);
	public void update(T light, List<Occluder> occluders);
	
	public void debugDraw(T light, Batch batch, ShapeRenderer shape);
	
	public Class<T> getLightClass();
}
