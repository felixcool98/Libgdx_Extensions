package com.felixcool98.gml;

import com.felixcool98.gml.Instruction.InstructionType;

public class InstructionBuilder {
	public Instruction parse(String text, int line) {
		InstructionType type = null;
		
		text = text.trim();
		
		if(text.endsWith(";"))
			text = text.substring(0, text.length()-1);
		
		if(text.contains("//"))
			type = InstructionType.Comment;
		else if(text.startsWith("if"))
			type = InstructionType.IF;
		else if(text.startsWith("else"))
			type = InstructionType.ELSE;
		else if(text.startsWith("for"))
			type = InstructionType.FOR;
		else if(text.startsWith("return"))
			type = InstructionType.RETURN;
		else if(text.contains("="))
			type = InstructionType.SetValue;
		else if(text.contains("{") || text.contains("}"))
			type = InstructionType.Brackets;
		else if(text.contains("("))
			type = InstructionType.FUNCTION;
		else if(text.contains(","))
			type = InstructionType.DECLARATION;
		
		return new Instruction(text, line, type);
	}
}
