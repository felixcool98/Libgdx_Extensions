package widgets.imageeditor.colors;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.widgets.imageeditor.colors.ColorPalette;

public class ColorPaletteTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("Color Palette Test");
		config.setWindowSizeLimits(1920, 1080, 1920, 1080);
		
		new Lwjgl3Application(new ColorPaletteTest(), config);
	}
	
	
	@Override
	public void create() {
		ColorPalette palette = new ColorPalette(Color.WHITE, Color.BROWN, Color.RED, null, Color.GOLD, null, null, Color.BLUE);
		palette.setRowSize(3);
		palette.setPosition(200, 200);
		
		getStage().addActor(palette);
	}
}
