package com.felixcool98.skeleton;

import java.util.ArrayList;
import java.util.List;

import com.felixcool98.skeleton.ISkeletalAnimation.SkeletalAnimationImpl;

public class SkeletalAnimation extends SkeletalAnimationImpl {
	private List<SkeletonInstance> images = new ArrayList<>();
	
	private float length = 0;
	
	private SkeletonInstance start;
	
	
	public SkeletalAnimation(SkeletonInstance start) {
		initalize(start);
	}
	
	protected void initalize(SkeletonInstance start) {
		this.start = start;
		images.add(start);
	}
	
	
	public void setStart(SkeletonInstance start) {
		this.start = start;
	}
	
	@Override
	public void addAngleToBone(String boneName, float angle, float time, float startTime) {
		if(start.getBone(boneName) == null)
			return;
		
		if(startTime + time > length)
			length = startTime + time;
		
		int startImage = (int) (startTime*getFPS());
		int images = (int) (time*getFPS());
		float anglePerImage = angle/(float) images;
		
		for(int i = 0; i < startImage+images; i++) {
			if(i < this.images.size())
				continue;
			
			this.images.add(this.images.get(i-1).clone());
			this.images.set(i, this.images.get(i-1).clone());
		}
		
		float currentAngle = startImage > 0 
				? this.images.get(startImage-1).getBone(boneName).getAngleOnParent() : 
					this.images.get(startImage).getBone(boneName).getAngleOnParent();
		for(int i = 0; i < images; i++) {
			this.images.get(startImage+i).getBone(boneName).setAngleOnParent(currentAngle);
			currentAngle += anglePerImage;
		}
	}
	
	
	public List<SkeletonInstance> getImages(){
		return images;
	}
}
