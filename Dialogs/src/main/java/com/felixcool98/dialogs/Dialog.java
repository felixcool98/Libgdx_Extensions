package com.felixcool98.dialogs;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.dialogs.options.Option;
import com.felixcool98.dialogs.options.OptionOpenDialog;
import com.felixcool98.utility.parsing.Parser;

public abstract class Dialog {
	private Character left;
	private Character right;
	
	private String leftImage;
	private String rightImage;
	
	private boolean leftSpeaks = true;
	
	private List<Option> options = new LinkedList<>();
	
	public Dialog() {
		this(null, null);
	}
	public Dialog(Character left, Character right) {
		this(left, "default", right, "default");
	}
	public Dialog(Character left, String leftImage,Character right, String rightImage) {
		this.left = left;
		this.leftImage = leftImage;
		
		this.right = right;
		this.rightImage = rightImage;
	}


	public Character getLeft() {
		return left;
	}
	public String getLeftImage() {
		return leftImage;
	}
	public boolean leftSpeaks() {
		return leftSpeaks;
	}

	public Character getRight() {
		return right;
	}
	public String getRightImage() {
		return rightImage;
	}
	public boolean rightSpeaks() {
		return !leftSpeaks;
	}

	public abstract String getText();
	public String getText(Parser parser) {
		return parser.parse(getText());
	}
	
	public List<Option> getOptions(){
		return options;
	}
	
	
	public void speakLeft() {
		leftSpeaks = true;
	}
	public void speakRight() {
		leftSpeaks = false;
	}
	
	public void addOption(Option option) {
		options.add(option);
	}
	public void addOptionOpenDialog(String text, Dialog dialog) {
		addOption(new OptionOpenDialog(text, dialog));
	}
}
