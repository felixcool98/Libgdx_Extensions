package com.felixcool98.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.felixcool98.gdxutility.ScissorUtils;

public class ScissoredTable extends Table {
	public ScissoredTable() {
		super();
	}
	
	
	@Override
	protected void drawChildren(Batch batch, float parentAlpha) {
		ScissorUtils.prepare(batch, this);
		
		super.drawChildren(batch, parentAlpha);
		
		ScissorUtils.after(getStage(), batch);
	}
}
