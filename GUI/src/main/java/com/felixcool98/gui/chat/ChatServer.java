package com.felixcool98.gui.chat;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.felixcool98.gui.chat.Message.ConnectionMessage;
import com.felixcool98.net.Server;

public class ChatServer extends Server {
	private List<ServerLogListener> serverLogListeners = new LinkedList<>();
	private Map<Integer, String> names = new HashMap<>();
	
	
	public ChatServer() {
		super();
		
		getServer().addListener(new Listener() {
			@Override
			public void received(Connection connection, Object object) {
				if(object instanceof ConnectionMessage) {
					names.put(connection.getID(), ((ConnectionMessage) object).getName());
				}
			}
			@Override
			public void disconnected(Connection connection) {
				sendMessage(new ConnectionMessage(names.get(connection.getID()), "disconnected"));
			}
		});
		
		addListener(new StartStopListener() {
			@Override
			public void serverStopped() {
				log("stopped server");
			}
			
			@Override
			public void serverStarted() {
				log("started server");
			}
		});
	}
	@Override
	public void registerClasses(Kryo kryo) {
		ChatKryo.prepareKry(kryo);
	}

	@Override
	public void received(Connection connection, Object object) {
		if(object instanceof Message) {
			sendMessage((Message) object);
		}
	}

	protected void sendMessage(Message message) {
		log(message.toString());
		
		getServer().sendToAllTCP(message);
	}
	
	private synchronized void log(String text) {
		for(ServerLogListener listener : serverLogListeners) {
			listener.serverLogged(text);
		}
	}
	public synchronized void addListener(ServerLogListener listener) {
		serverLogListeners.add(listener);
	}
	
	public static interface ServerLogListener {
		public void serverLogged(String text);
	}
}
