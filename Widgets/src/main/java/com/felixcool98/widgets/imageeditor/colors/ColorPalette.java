package com.felixcool98.widgets.imageeditor.colors;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.widgets.Widgets;
import com.felixcool98.widgets.imageeditor.colors.ColorField.SelectedColorListener;

/**
 * A color palette for displaying and selecting multiple colors
 * 
 * @author felixcool98
 */
public class ColorPalette extends Table implements SelectedColorListener {
	/**The list of selected color listeners**/
	private List<SelectedColorListener> listeners = new LinkedList<>();
	
	/**The amount of {@link ColorField}s per row**/
	private int rowSize = 2;
	/**The size of the {@link ColorField}s**/
	private float fieldSize = 16;
	/**The padding of the {@link ColorField}s**/
	private float fieldPad = 2;
	
	/**Used to determine when to start a new row**/
	private int currentColor = 0;
	
	/**a list of all the fields contained in the palette**/
	private List<ColorField> fields = new LinkedList<>();
	/**a list of all the colors contained in the palette**/
	private List<Color> colors = new LinkedList<>();
	
	/**will call {@link ColorPalette#rebuild()} in validate if it is set to true**/
	private boolean needsRebuild = false;
	
	
	/**
	 * creates a empty color palette<br>
	 * sets the style via the default {@link Widgets#getSkin()} skin
	 */
	public ColorPalette() {
		this(new Color[0]);
	}
	/**
	 * creates a color palette<br>
	 * adds all the colors via {@link ColorPalette#addColors(Color...)} to the palette<br>
	 * sets the style via the {@link Widgets#getSkin()} skins default style
	 * 
	 * @param colors colors to add
	 */
	public ColorPalette(Color... colors) {
		this(Widgets.getSkin(), colors);
	}
	/**
	 * creates a color palette<br>
	 * sets the style via the {@link Widgets#getSkin()} skins default style<br>
	 * adds all the colors via {@link ColorPalette#addColors(Color...)} to the palette
	 * 
	 * @param skin the skin to use(will use the default style)
	 * @param colors colors to add
	 */
	public ColorPalette(Skin skin, Color... colors) {
		this(skin.get(ColorPaletteStyle.class), colors);
	}
	/**
	 * creates a color palette<br>
	 * sets the style to the provided style<br>
	 * adds all the colors via {@link ColorPalette#addColors(Color...)} to the palette
	 * 
	 * @param style the style to use
	 * @param colors colors to add
	 */
	public ColorPalette(ColorPaletteStyle style, Color... colors) {
		super();
		
		setStyle(style);
		addColors(colors);
	}
	
	
	/**
	 * sets the style
	 * 
	 * @param style the style to use
	 */
	public void setStyle(ColorPaletteStyle style) {
		if(style == null)
			return;
		
		setRowSize(style.rowSize);
		setFieldPad(style.fieldPad);
		setFieldSize(style.fieldSize);
		
		if(style.background != null)
			setBackground(style.background);
	}
	/**
	 * sets the {@link ColorPalette#rowSize}<br>
	 * will do nothing if the size is smaller than 1
	 * 
	 * @param size the value to set the rowsize to
	 */
	public void setRowSize(int size) {
		if(size < 1)
			return;
		
		rowSize = size;
		
		needsRebuild = true;
	}
	/**
	 * sets the {@link ColorPalette#fieldPad}<br>
	 * 
	 * @param pad the value to set the fieldpad to
	 */
	public void setFieldPad(float pad) {
		fieldPad = pad;
		
		needsRebuild = true;
	}
	/**
	 * sets the {@link ColorPalette#fieldSize}<br>
	 * 
	 * @param size the value to set the fieldsize to
	 */
	public void setFieldSize(float size) {
		fieldSize = size;
		
		needsRebuild = true;
	}
	
	
	/**
	 * convenience method<br>
	 * will call {@link ColorPalette#addColor(Color)} for every color in the list
	 * 
	 * @param colors the colors to add
	 */
	public void addColors(List<Color> colors) {
		for(Color color : colors) {
			addColor(color);
		}
	}
	/**
	 * convenience method<br>
	 * will call {@link ColorPalette#addColor(Color)} for every argument
	 * 
	 * @param colors the colors to add
	 */
	public void addColors(Color... colors) {
		for(Color color : colors) {
			addColor(color);
		}
	}
	public void addColor(Color color) {
		if(needsRebuild) {
			colors.add(color);
			return;
		}
		
		if(currentColor%rowSize == 0 && currentColor != 0) {
			row();
		}
		
		colors.add(color);
		
		if(color == null) {
			this.add().size(fieldSize).pad(fieldPad);
			
			currentColor++;
			
			return;
		}
		
		ColorField field = new ColorField();
		field.setSize(fieldSize, fieldSize);
		field.setColor(color);
		field.addListener(new SelectedColorListener() {
			@Override
			public void selectedColor(Color newColor) {
				ColorPalette.this.selectedColor(newColor);
			}
		});
		
		fields.add(field);
		
		this.add(field).size(fieldSize).pad(fieldPad);
		
		currentColor++;
	}
	/**
	 * clears the {@link ColorPalette#fields} list and disposes of all fields
	 */
	private void clearColorFields() {
		for(ColorField field : fields) {
			field.dispose();
		}
		
		fields.clear();
	}
	/**
	 * clears all colors and adds the again
	 */
	private void rebuild() {
		needsRebuild = false;
		currentColor = 0;
		
		List<Color> colors = new LinkedList<>(this.colors);
		
		clearColors();
		
		addColors(colors);
		
		
	}
	/**
	 * removes all colors from the palette
	 */
	public void clearColors() {
		clearChildren();
		clearColorFields();
		colors.clear();
	}
	
	
	/**
	 * adds a {@link SelectedColorListener} to this palette<br>
	 * {@link SelectedColorListener#selectedColor(Color)} will be called when any color in the palette was selected
	 * 
	 * @param listener the listener to add
	 */
	public void addListener(SelectedColorListener listener) {
		listeners.add(listener);
	}
	public void removeListener(SelectedColorListener listener) {
		listeners.remove(listener);
	}


	@Override
	@Deprecated
	public void selectedColor(Color newColor) {
		for(SelectedColorListener listener : listeners) {
			listener.selectedColor(newColor);
		}
	}
	@Override
	public void validate() {
		super.validate();
		
		if(needsRebuild)
			rebuild();
	}
	
	
	/**
	 * Style for colorpalette
	 * 
	 * @author felixcool98
	 */
	public static class ColorPaletteStyle{
		/**background of the palette**/
		public Drawable background;
		/**{@link ColorField}s per row<br>default = 2**/
		public int rowSize = 2;
		/**size of the {@link ColorField}s<br>default = 16**/
		public float fieldSize = 16;
		/**the padding of the {@link ColorField}s<br>default = 2**/
		public float fieldPad = 2;
	}
}
