package skeleton;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.skeleton.image.ImageSkeletonBuilder;
import com.felixcool98.skeleton.image.ImageSkeletonInstance;

public class Impl extends ImprovedGameState {
	private ImageSkeletonInstance skeleton;
	
	@Override
	public void create() {
		skeleton = new ImageSkeletonBuilder()
				.addBone("bone")
				.setImage("tests/skeleton/Arrow.png")
				.setData(90, 100, 0)
				.setImageData(-10, 0, 20, 100, -90)
				.setDepth(1)
				
				.addBone("bone2")
				.setImage("tests/skeleton/Arrow.png")
				.setData(90, 50, 100)
				.setImageData(-15, 10, 30, 20, -90)
				
				.build();
	}
	
	@Override
	public void renderGame() {
		ShapeRenderer renderer = new ShapeRenderer();

		skeleton.render(new SpriteBatch(), 200, 200);
		renderer.setColor(Color.RED);
		skeleton.renderDebugImageOutlines(renderer, 200, 200, 0);
		renderer.setColor(Color.WHITE);
		skeleton.renderDebugLines(renderer, 200, 200, 0);
	}
}
