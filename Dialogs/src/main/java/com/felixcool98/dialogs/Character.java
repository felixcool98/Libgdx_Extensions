package com.felixcool98.dialogs;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.felixcool98.dialogs.res.DefaultValues;

public class Character {
	private Map<String, TextureRegion> images = new HashMap<>();
	private String name;
	
	
	public Character(String name) {
		this(name, null);
	}
	public Character(String name, TextureRegion defaultImage) {
		this.name = name;
		this.images.put("default", defaultImage);
	}
	
	
	public void useDefaultImage() {
		this.images.put("default", DefaultValues.DEFAULT_CHARACTER_IMAGE);
	}
	
	
	public void putImage(String name, TextureRegion image) {
		images.put(name, image);
	}
	
	
	public String getName() {
		return name;
	}
	public TextureRegion getImage(String name) {
		return images.get(name);
	}
}
