package com.felixcool98.logging.logtype;

import com.felixcool98.utility.filter.Filter;

public class LogLevelFilter implements Filter<Integer> {
	private int min;
	
	
	public LogLevelFilter(int min) {
		this.min = min;
	}
	
	
	@Override
	public boolean valid(Integer object) {
		if(object < min)
			return false;
		
		return true;
	}

}
