package com.felixcool98.gui.texteditor;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;

public class TextHighlight {
	private List<HighlightElement> highlights = new LinkedList<>();
	
	
	public void add(HighlightElement highlight) {
		highlights.add(highlight);
	}
	
	
	public List<HighlightElement> getHighlights(){
		return highlights;
	}
	
	
	public static class HighlightElement{
		private String text;
		private Color color;
		
		
		public HighlightElement(String text, Color color) {
			this.text = text;
			this.color = color;
		}
		
		
		public String getText() {
			return text;
		}
		public Color getColor() {
			return color;
		}
	}
}
