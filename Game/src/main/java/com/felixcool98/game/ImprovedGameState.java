package com.felixcool98.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.felixcool98.gdxutility.GdxUtils;

public abstract class ImprovedGameState extends GameStateAdapter {
	private Stage stage;
	private boolean autoActDrawStage = true;
	
	private Color clearColor = Color.WHITE;
	private boolean autoClearScreen = true;
	
	private InputMultiplexer multiplexer = new InputMultiplexer();
	
	
	public void setStage(Stage stage) {
		setStage(stage, true);
	}
	public void setStage(Stage stage, boolean addToMultiplexer) {
		this.stage = stage;
		
		if(addToMultiplexer)
			multiplexer.addProcessor(stage);
	}
	public void setClearColor(Color clearColor) {
		this.clearColor = clearColor;
	}
	
	
	public Stage getStage() {
		return stage;
	}
	public Color getClearColor() {
		return clearColor;
	}
	public InputMultiplexer getMultiplexer() {
		return multiplexer;
	}
	
	
	//=====================================
	// create
	//=====================================
	
	/**
	 * sets up input processor and stage<br>
	 * calls {@link ImprovedGameState#created()} afterwards<br>
	 * instead of overwriting this using created is advised
	 */
	@Deprecated
	@Override
	public void create() {
		setStage(new Stage());
		
		Gdx.input.setInputProcessor(getMultiplexer());
		
		created();
	}
	public void created() {
		
	}
	
	
	//=====================================
	// rendering
	//=====================================
	
	@Deprecated
	@Override
	public void render() {
		if(autoClearScreen)
			clearScreen();
		
		renderGame();
		if(autoActDrawStage)
			renderAndActStage();
	}
	public void clearScreen() {
		GdxUtils.clearScreen(clearColor);
	}
	public void renderAndActStage() {
		if(stage == null)
			return;
		
		stage.act();
		stage.draw();
	}
	public void renderGame() {
		
	}
	
	
	public void disableAutoScreenClear() {
		autoClearScreen = false;
	}
	public void disableAutoStageActAndRender() {
		autoActDrawStage = false;
	}
	
	
	@Override
	public void resize(int width, int height) {
		if(stage == null)
			return;
		
		getStage().getViewport().setScreenSize(width, height);
	}
}
