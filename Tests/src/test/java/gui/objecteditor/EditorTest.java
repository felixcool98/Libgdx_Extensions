package gui.objecteditor;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gui.objecteditor.ObjectEditor;
import com.felixcool98.utility.fields.Description;
import com.felixcool98.utility.fields.Getter;
import com.felixcool98.utility.fields.Ignore;
import com.felixcool98.utility.fields.Max;
import com.felixcool98.utility.fields.Min;
import com.felixcool98.utility.fields.Name;
import com.felixcool98.utility.fields.Setter;

public class EditorTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("EditorTest");
		
		config.setWindowSizeLimits(700, 500, 700, 500);
		
		new Lwjgl3Application(new EditorTest(), config);
	}
	
	
	@Override
	public void created() {
		ObjectEditor editor = new ObjectEditor();
		
		editor.setObject(new TestComponent());
		
		Table table = new Table();{
			table.align(Align.bottomLeft);
			
			table.add(editor).align(Align.bottomLeft);
		}getStage().addActor(table);
		
		getStage().setDebugAll(true);
	}
	
	
	public static class TestComponent {
		@Getter(methodName = "getTestBoolean")@Setter(methodName = "setTestBoolean")@Name(fieldName = "test boolean")
		private boolean testBoolean = true;
		@Ignore
		public boolean ingoredBoolean = false;
		public boolean publicBooelan = false;
		
		public int testInteger = 1000;
		
		@Description(description = "this is a test description<br>"
				+ "line break"
				+ "<p>"
				+ "paragraph"
				+ "</p>")
		public float floattest = 888;
		
		@Min(min = 2)@Max(max = 20)
		public String testString = "hello world";
		
		public boolean getTestBoolean() {
			return testBoolean;
		}
		public void setTestBoolean(boolean value) {
			testBoolean = value;
		}
	}
}
