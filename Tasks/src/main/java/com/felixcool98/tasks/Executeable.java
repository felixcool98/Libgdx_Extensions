package com.felixcool98.tasks;

public interface Executeable {
	public String getDisplayText();
	public void execute();
	public float getProgress();
	public void addListener(LTaskExecuted listener);
}
