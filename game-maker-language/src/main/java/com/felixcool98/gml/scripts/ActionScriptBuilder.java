package com.felixcool98.gml.scripts;

import com.felixcool98.gml.Instruction;
import com.felixcool98.gml.Instruction.InstructionType;
import com.felixcool98.gml.Scope.ScopeType;
import com.felixcool98.gml.actions.Action;
import com.felixcool98.gml.actions.SetValueToConstant;
import com.felixcool98.gml.actions.SetValueToMath;
import com.felixcool98.gml.maths.MathFunction;
import com.felixcool98.gml.maths.parser.MathParser;

public class ActionScriptBuilder {
	private ActionScript script;
	
	
	public Script build(Instruction[] instructions) {
		script = new ActionScript();
		
		for(Instruction instruction : instructions) {
			if(instruction.getType() == InstructionType.Comment) {
				if(instruction.getText().startsWith("///") && instruction.getLine() == 0) {
					parseHeader(instruction);
				}
			}else {
				Action action = createAction(instruction);
				
				if(action != null)
					script.addAction(action);
			}
		}
		
		return script;
	}
	
	private void parseHeader(Instruction instruction) {
		String text = instruction.getText();
		
		text = text.replaceFirst("///", "").trim();
		
		if(!text.contains("(") || ! text.contains(")"))
			System.err.println(instruction.getLine() + ": method description is wrong | " + instruction.getText());
	
		script.setName(text.substring(0, text.indexOf("(")));
		
		script.setArguments(text.substring(text.indexOf("(") + 1, text.indexOf(")")).split(","));
	}
	
	private Action createAction(Instruction instruction) {
		if(instruction.getType() == InstructionType.SetValue) {
			if(instruction.getText().contains("\"") || isNumber(instruction.getTextBehindEquals()))
				return createSetValueToConstant(instruction);
			else
				return createSetValueToMath(instruction);
		}
		
		return null;
	}
	
	
	private Action createSetValueToConstant(Instruction instruction) {
		String text = instruction.getText();
		
		String name = text.substring(0, text.indexOf("=")).trim();
		
		String rest = instruction.getTextBehindEquals();
		
		Object to = null;
		
		if(rest.startsWith("\""))
			to = rest.substring(1, rest.length()-1);
		else
			to = toNumber(rest);
		
		ScopeType type = ScopeType.AUTO;
		
		if(name.startsWith("var")) {
			type = ScopeType.LOCAL;
		}
		
		return new SetValueToConstant(name, to, type);
	}
	private Action createSetValueToMath(Instruction instruction) {
		String text = instruction.getText();
		
		String name = text.substring(0, text.indexOf("=")).trim();
		
		String rest = instruction.getTextBehindEquals();
		
		MathFunction function = new MathParser().parse(rest);
		
		ScopeType type = ScopeType.AUTO;
		
		if(name.startsWith("var")) {
			type = ScopeType.LOCAL;
		}
		
		return new SetValueToMath(name, function, type);
	}
	
	
	private boolean isNumber(String text) {
		try {
			Integer.parseInt(text);
		} catch (Exception e) {
			try {
				Float.parseFloat(text);
			} catch (Exception e2) {
				return false;
			}
		}
		
		return true;
	}
	private Object toNumber(String text) {
		try {
			return Integer.parseInt(text);
		} catch (Exception e) {
			return Float.parseFloat(text);
		}
	}
}
