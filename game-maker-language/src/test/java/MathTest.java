import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.maths.Adder;
import com.felixcool98.gml.maths.Multiplier;
import com.felixcool98.gml.maths.Setter;
import com.felixcool98.gml.maths.get.Const;
import com.felixcool98.gml.maths.get.VarGetter;
import com.felixcool98.gml.objects.GMLObject;

public class MathTest {

	@Test
	public void test() {
		Scope global = new Scope();
		Scope object = new Scope();
		Scope local = new Scope();
		
		local.add("x", new GMLObject(100));
		
		new Setter("x", new Adder(new VarGetter("x"), new Const(new GMLObject(200)))).set(global, object, local);
		new Setter("x", new Multiplier(new Const(new GMLObject(2.5f)), new VarGetter("x"))).set(global, object, local);;
		
		
		assertEquals(750f, new VarGetter("x").get(global, object, local).getValue());
	}

}
