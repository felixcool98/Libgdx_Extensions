package com.felixcool98.logging.logtype;

import java.util.HashSet;

public class LogType {
	public static final LogType EXCEPTION = new LogType(100, "Exception");
	public static final LogType WARNING = new LogType(50, "Warning");
	public static final LogType MESSAGE = new LogType(25, "Message");
	
	
	public final HashSet<String> tags;
	public final int level;
	
	
	public LogType(int level, String...tags) {
		this(level);
		
		addTags(tags);
	}
	public LogType(int level) {
		tags = new HashSet<String>();
		this.level = level;
	}
	
	
	public void addTags(String... tags) {
		if(tags == null)
			return;
		
		for(String tag : tags) {
			this.tags.add(tag);
		}
	}
	
	
	public boolean hasTag(String tag) {
		return tags.contains(tag);
	}
}
