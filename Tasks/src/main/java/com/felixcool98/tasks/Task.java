package com.felixcool98.tasks;

import java.util.LinkedList;
import java.util.List;

public abstract class Task implements Executeable {
	private String displayText;
	
	private List<LTaskExecuted> listeners = new LinkedList<>();
	
	
	public Task(String displayText) {
		this.displayText = displayText;
	}
	
	
	@Override
	public String getDisplayText() {
		return displayText;
	}
	@Override
	public float getProgress() {
		return 100;
	}
	
	
	public void taskExecuted() {
		for(LTaskExecuted listener : listeners) {
			listener.done(getProgress());
		}
	}
	@Override
	public void addListener(LTaskExecuted listener) {
		listeners.add(listener);
	}
}
