package com.felixcool98.widgets.imageeditor;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.felixcool98.widgets.PixmapUtils;
import com.felixcool98.widgets.imageeditor.EditorToolBar.EditorToolBarStyle;
import com.felixcool98.widgets.imageeditor.canvas.DrawableCanvas;
import com.felixcool98.widgets.imageeditor.canvas.EditorAction;
import com.felixcool98.widgets.imageeditor.colors.ColorField;
import com.felixcool98.widgets.imageeditor.colors.ColorPalette;
import com.felixcool98.widgets.imageeditor.colors.ColorField.SelectedColorListener;
import com.felixcool98.widgets.imageeditor.colors.ColorPalette.ColorPaletteStyle;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisTextField.TextFieldFilter;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.kotcrab.vis.ui.widget.color.ColorPicker;
import com.kotcrab.vis.ui.widget.color.ColorPickerListener;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserStyle;
import com.kotcrab.vis.ui.widget.file.FileChooser.Mode;
import com.kotcrab.vis.ui.widget.file.FileChooser.SelectionMode;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import com.kotcrab.vis.ui.widget.file.FileTypeFilter;

public class ImageEditor extends Table {
	private float buttonSize = 32;
	
	private FileChooser fileChooser = new FileChooser(new FileHandle(""), Mode.SAVE);;
	private ColorPicker colorPicker = new ColorPicker();
	
	private ColorField colorButton;
	
	
	private int drawTableWidth = 400;
	private int drawTableHeight = 400;
	
	private int newImageWidth = 16;
	private int newImageHeight = 16;
	
	private Table drawTable;
	private DrawableCanvas canvas;
	
	private ImageEditorStyle style;
	
	
	public ImageEditor(ImageEditorStyle style) {
		super(VisUI.getSkin());
		
		this.style = style;
		canvas = new DrawableCanvas();
		canvas.setSize(drawTableWidth, drawTableHeight);
		createMenuBar();
		createColorPicker();
		createSideBar();
		createDrawTable();
		createColorPalette();
	}
	
	
	private void createMenuBar() {
		Table menuBar = new Table();
		
		VisImageButton newImageButton = new VisImageButton(style.newImageDrawable);
		newImageButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				VisTable table = new VisTable();
				
				VisLabel width = new VisLabel("width");
				VisTextField widthTextField = new VisTextField("" + newImageWidth);
				widthTextField.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
				table.add(width);
				table.add(widthTextField).align(Align.left);
				table.row();
				
				
				VisLabel height = new VisLabel("height");
				VisTextField heightTextField = new VisTextField("" + newImageHeight);
				heightTextField.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
				table.add(height);
				table.add(heightTextField).align(Align.left);
				
				table.row();
				
				VisWindow window = new VisWindow("New Image");
				
				VisTextButton create = new VisTextButton("create");
				VisTextButton cancel = new VisTextButton("cancel");
				
				VisLabel problemText = new VisLabel();
				problemText.setColor(Color.RED);
				
				create.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						if(widthTextField.getText().equals("") && heightTextField.getText().equals("")) {
							problemText.setText("width and height can't be empty");
							window.pack();
							
							return;
						}else if(widthTextField.getText().equals("")) {
							problemText.setText("width can't be empty");
							window.pack();
							
							return;
						}else if(heightTextField.getText().equals("")) {
							problemText.setText("height can't be empty");
							window.pack();
							
							return;
						}
						
						newImageWidth = Integer.parseInt(widthTextField.getText());
						newImageHeight = Integer.parseInt(heightTextField.getText());
						ImageEditor.this.canvas.newImage(newImageWidth, newImageHeight);
						
						window.remove();
					}
				});
				table.add(create).pad(3);
				
				cancel.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						window.remove();
					}
				});
				table.add(cancel).pad(3);
				
				table.add(problemText);
				
				window.add(table);
				window.pack();
				
				window.setPosition(ImageEditor.this.getStage().getWidth()/2, ImageEditor.this.getStage().getHeight()/2);
				
				ImageEditor.this.getStage().addActor(window);
			}
		});
		
		menuBar.add(newImageButton).size(buttonSize).align(Align.left);
		
		FileChooserStyle style = VisUI.getSkin().get("default", FileChooserStyle.class);
		
		VisImageButton loadButton = new VisImageButton(style.iconFolderParent);
		loadButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				loadImage();
			}
		});
		
		menuBar.add(loadButton).size(buttonSize).align(Align.left);
		
		VisImageButton saveButton = new VisImageButton(this.style.saveImageDrawable);
		saveButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				saveImage();
			}
		});
		
		menuBar.add(saveButton).size(buttonSize).align(Align.left);
		
		this.add(menuBar).row();
	}
	private void createColorPicker() {
		colorPicker.setListener(new ColorPickerListener() {
			@Override
			public void reset(Color previousColor, Color newColor) {
				canvas.setColor(newColor);
				colorButton.setColor(newColor);
			}
			
			@Override
			public void finished(Color newColor) {
				canvas.setColor(newColor);
				colorButton.setColor(newColor);
			}
			
			@Override
			public void changed(Color newColor) {
				canvas.setColor(newColor);
				colorButton.setColor(newColor);
			}
			
			@Override
			public void canceled(Color oldColor) {
				canvas.setColor(oldColor);
				colorButton.setColor(oldColor);
			}
		});
	}
	
	
	//================================================================================================================================================
	// sidebar
	//================================================================================================================================================
	private void createSideBar() {
		Table sidebar = new Table();
		
		createColorButton(sidebar);
		createTools(sidebar);
		
		this.add(sidebar);
	}
	private void createColorButton(Table table) {
		colorButton = new ColorField();
		colorButton.setSize(buttonSize, buttonSize);
		
		colorButton.addListener(new SelectedColorListener() {
			@Override
			public void selectedColor(Color newColor) {
				colorPicker.setColor(colorButton.getColor());
				getStage().addActor(colorPicker.fadeIn());
			}
		});
		
		table.add(colorButton).size(buttonSize).align(Align.top).pad(3);
	}
	private void createTools(Table table) {
		EditorToolBar toolBar = new EditorToolBar();
		toolBar.setStyle(style.editorToolBarStyle);
		toolBar.setCanvas(this.canvas);
		
		List<EditorTool> tools = new LinkedList<>();
		
		EditorTool fill = new EditorTool(style.fillToolDrawable);
		fill.setCanvas(this.canvas);
		fill.setAction(new EditorAction.Fill());
		tools.add(fill);
		
		EditorTool point = new EditorTool(style.pointToolDrawable);
		point.setCanvas(this.canvas);
		point.setAction(new EditorAction.Point());
		tools.add(point);
		
		EditorTool line = new EditorTool(style.lineToolDrawable);
		line.setCanvas(this.canvas);
		line.setAction(new EditorAction.Line());
		tools.add(line);
		
		EditorTool zoomOut = new EditorTool(style.zoomOutToolDrawable);
		zoomOut.setCanvas(this.canvas);
		zoomOut.setAction(new EditorAction.ZoomOut());
		tools.add(zoomOut);
		
		EditorTool zoomIn = new EditorTool(style.zoomInToolDrawable);
		zoomIn.setCanvas(this.canvas);
		zoomIn.setAction(new EditorAction.ZoomIn());
		tools.add(zoomIn);
		
		EditorTool maxZoomOut = new EditorTool(style.zoomMaxToolDrawable);
		maxZoomOut.setCanvas(this.canvas);
		maxZoomOut.setAction(new EditorAction.MaxZoomOut());
		tools.add(maxZoomOut);
		
		EditorTool move = new EditorTool(style.moveToolDrawable);
		move.setCanvas(this.canvas);
		move.setAction(new EditorAction.Move());
		tools.add(move);

		toolBar.setEditorTools(tools);
		
		table.row();
		table.add(toolBar);
	}
	
	
	
	
	
	
	private void createDrawTable() {
		drawTable = new Table();
		drawTable.setTouchable(Touchable.enabled);
		
		colorButton.setColor(canvas.getDrawColor());
		
		this.add(drawTable).size(drawTableWidth, drawTableHeight).pad(3);
		
		drawTable.add(canvas);
		drawTable.toBack();
	}
	private void createColorPalette() {
		ColorPaletteStyle style = new ColorPaletteStyle();{
			style.fieldPad = 2;
			style.fieldSize = 20;
			style.rowSize = 6;
		}
		ColorPalette palette = new ColorPalette(style,
				Color.CLEAR, Color.WHITE, Color.LIGHT_GRAY, Color.GRAY, Color.DARK_GRAY, Color.BLACK, null,
				Color.GREEN, Color.FOREST, Color.CHARTREUSE, Color.OLIVE, null,
				Color.CYAN, Color.BLUE,  Color.NAVY, null, 
				Color.YELLOW, Color.GOLD, null,
				Color.ORANGE, Color.CORAL, Color.BROWN, Color.RED);
		palette.addListener(new SelectedColorListener() {
			@Override
			public void selectedColor(Color newColor) {
				canvas.setColor(newColor);
				colorButton.setColor(newColor);
			}
		});
		
		add(palette);
	}
	
	
	//================================================================================================================================================
	// save images
	//================================================================================================================================================
	public void saveImage() {
		fileChooser.setSelectionMode(SelectionMode.FILES_AND_DIRECTORIES);
		fileChooser.setMode(Mode.SAVE);
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setIconProvider(new FileChooser.DefaultFileIconProvider(fileChooser));
		FileChooser.setSaveLastDirectory(true);
		
		FileTypeFilter typeFilter = new FileTypeFilter(false);
		typeFilter.addRule("png", "png");
		
		fileChooser.setFileTypeFilter(typeFilter);
		
		fileChooser.setDefaultFileName("image.png");
		getStage().addActor(fileChooser.fadeIn());
		
		fileChooser.setListener(new FileChooserAdapter() {
			@Override
			public void selected(Array<FileHandle> files) {
				super.selected(files);
				
				saveImage(files.get(0));
			}
		});
	}
	
	public void saveImage(File file) {
		saveImage(new FileHandle(file));
	}
	public void saveImage(FileHandle file) {
		canvas.save(file);
	}
	
	
	//================================================================================================================================================
	// load images
	//================================================================================================================================================
	public void loadImage() {
		fileChooser.setSelectionMode(SelectionMode.FILES);
		fileChooser.setMode(Mode.OPEN);
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setIconProvider(new FileChooser.DefaultFileIconProvider(fileChooser));
		FileChooser.setSaveLastDirectory(true);
		
		FileTypeFilter typeFilter = new FileTypeFilter(false);
		typeFilter.addRule("png", "png");
		
		fileChooser.setFileTypeFilter(typeFilter);
		
		fileChooser.setDefaultFileName("image.png");
		getStage().addActor(fileChooser.fadeIn());
		
		fileChooser.setListener(new FileChooserAdapter() {
			@Override
			public void selected(Array<FileHandle> files) {
				super.selected(files);
				
				loadImage(files.get(0));
			}
		});
	}
	public void loadImage(FileHandle file) {
		Texture texture = new Texture(file);
		
		texture.getTextureData().prepare();
		Pixmap pixmap = texture.getTextureData().consumePixmap();
		Pixmap load = PixmapUtils.creatFlippedPixMap(pixmap);
		
		canvas.setImage(load);
	}
	
	
	public static class ImageEditorStyle {
		public EditorToolBarStyle editorToolBarStyle;
		
		public Drawable newImageDrawable;
		public Drawable saveImageDrawable;
		
		public Drawable fillToolDrawable;
		public Drawable pointToolDrawable;
		public Drawable lineToolDrawable;
		public Drawable zoomInToolDrawable;
		public Drawable zoomOutToolDrawable;
		public Drawable zoomMaxToolDrawable;
		public Drawable moveToolDrawable;
	}
}
