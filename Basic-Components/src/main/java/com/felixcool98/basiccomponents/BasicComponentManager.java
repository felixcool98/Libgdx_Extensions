package com.felixcool98.basiccomponents;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.basiccomponents.components.ComponentDrawable;
import com.felixcool98.basiccomponents.components.ConnectedDrawables;
import com.felixcool98.basiccomponents.components.InstanceDrawable;
import com.felixcool98.basiccomponents.components.Position;
import com.felixcool98.basiccomponents.components.Rotation;
import com.felixcool98.basiccomponents.components.Size;
import com.felixcool98.components.ComponentNotFoundException;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.ComponentManager;
import com.felixcool98.utility.values.AdvancedDirection;

public class BasicComponentManager extends ComponentManager {
	public BasicComponentManager() {
		super();
	}
	
	
	@Override
	public BasicComponentDataManager createDataManager(CreationData data) {
		return new BasicComponentDataManager(this, data);
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	
	//size
	
	public void setSize(float width, float height) {
		if(!hasSize())
			addComponent(new Size());
		
		getCSize().set(width, height);
	}
	public void setWidth(float width) {
		if(!hasSize())
			addComponent(new Size());
		
		getCSize().setWidth(width);
	}
	public void setHeight(float height) {
		if(!hasSize())
			addComponent(new Size());
		
		getCSize().setHeight(height);
	}
	
	//rotation
	
	public void setRotation(float rotation) {
		if(!hasComponent(Rotation.class))
			addComponent(new Rotation());
		
		getCRotation().setRotation(rotation);
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	public boolean hasSize() {
		return hasComponent(Size.class);
	}
	public boolean hasRotation() {
		return hasComponent(Rotation.class);
	}
	public boolean hasComponentDrawable() {
		return hasComponent(ComponentDrawable.class);
	}
	
	//data
	
	public boolean hasPosition() {
		return hasComponent(Position.class);
	}
	
	//other
	
	public boolean hasDrawable() {
		return hasComponentDrawable() || hasComponent(ConnectedDrawables.class) || hasComponent(InstanceDrawable.class);
	}
	
	//======================================================================
	// getter
	//======================================================================
	
	//size
	
	public Size getCSize() {
		if(!hasSize())
			throw new ComponentNotFoundException(Size.class);
		
		return getComponent(Size.class);
	}
	public float getWidth() {
		if(!hasSize())
			throw new ComponentNotFoundException(Size.class);
		
		return getCSize().getWidth();
	}
	public float getHeight() {
		if(!hasSize())
			throw new ComponentNotFoundException(Size.class);
		
		return getCSize().getHeight();
	}
	
	
	//rotation
	
	public Rotation getCRotation() {
		return getComponent(Rotation.class);
	}
	public float getRotation() {
		if(hasRotation())
			return getCRotation().getRotation();
		
		return 0;
	}
	
	//Drawable
	
	public ComponentDrawable getComponentDrawable() {
		return getComponent(ComponentDrawable.class);
	}
	public Drawable getDrawable() {
		if(hasComponentDrawable())
			return getComponentDrawable().getDrawable();
		
		return null;
	}
	
	public ConnectedDrawables getConnectedDrawables() {
		return getComponent(ConnectedDrawables.class);
	}
	public Drawable getDrawable(AdvancedDirection surrounding) {
		if(!hasComponent(ConnectedDrawables.class))
			throw new ComponentNotFoundException(ConnectedDrawables.class);
		
		return getConnectedDrawables().getDrawable(surrounding);
	}
}
