package com.felixcool98.gml.scripts;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.objects.GMLObject;

public interface Script {
	public default GMLObject execute(Scope global, Scope object, Object ...objects) {
		GMLObject[] gmlobjects = new GMLObject[objects.length];
		
		for(int i = 0; i < objects.length; i++) {
			gmlobjects[i] = new GMLObject(objects[i]);
		}
			
		
		return execute(global, object, gmlobjects);
	};
	public GMLObject execute(Scope global, Scope object, GMLObject ...objects);
}
