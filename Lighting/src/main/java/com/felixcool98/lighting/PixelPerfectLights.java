package com.felixcool98.lighting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.lighting.lightsources.Light;
import com.felixcool98.lighting.occluders.Occluder;
import com.felixcool98.lighting.pixelperfect.LightHandler;

/**
 * Light system implementation of https://github.com/mattdesl/lwjgl-basics/wiki/2D-Pixel-Perfect-Shadows
 * 
 * @author felixcool98
 */
public class PixelPerfectLights implements LightSystem {
	private Map<Class<? extends Light>, LightHandler<?>> handlers = new HashMap<>();
	
	
	public void update(List<Light> lights, List<Occluder> occluders) {
		for(Light light : lights) {
			update(light, occluders);
		}
	}
	public void update(Light light, List<Occluder> occluders) {
		getHandler(light).update(light, occluders);
	}
	
	public void render(List<Light> lights, Batch batch) {
		for(Light light : lights) {
			render(light, batch);
		}
	}
	public void render(Light light, Batch batch) {
		getHandler(light).render(light, batch);
	}
	
	
	public void register(LightHandler<?> handler) {
		handlers.put(handler.getLightClass(), handler);
	}
	
	
	public void debugDraw(Light light, Batch batch, ShapeRenderer shape) {
		getHandler(light).debugDraw(light, batch, shape);
	}
	
	
	@SuppressWarnings("unchecked")
	private <T extends Light> LightHandler<T> getHandler(T light) {
		return (LightHandler<T>) handlers.get(light.getClass());
	}
}
