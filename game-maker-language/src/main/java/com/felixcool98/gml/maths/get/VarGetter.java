package com.felixcool98.gml.maths.get;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.Scope.ScopeType;
import com.felixcool98.gml.maths.MathFunction;
import com.felixcool98.gml.objects.GMLObject;

public class VarGetter implements MathFunction {
	private String name;
	private ScopeType type;
	
	
	public VarGetter(String name) {
		if(name.isEmpty())
			throw new IllegalArgumentException("name can't be empty");
		
		this.name = name.replaceFirst("global.", "");
		this.type = name.startsWith("global.") ? ScopeType.GLOBAL : ScopeType.AUTO;
	}
	
	
	@Override
	public GMLObject get(Scope global, Scope object, Scope local) {
		Scope scope = Scope.getScope(global, object, local, type, name);
		
		GMLObject get = scope.get(name);
		
		if(get != null)
			return get;
		else
			throw new IllegalArgumentException("tried getting " + name + " but it was not initialized");
	}
	
	
	public String getName() {
		return name;
	}
	
	
	@Override
	public String toString() {
		return "x" + super.hashCode();
	}
}
