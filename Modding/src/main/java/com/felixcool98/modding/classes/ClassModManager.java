package com.felixcool98.modding.classes;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.logging.Logs;
import com.felixcool98.logging.logtype.LogType;
import com.felixcool98.modding.ModException;
import com.felixcool98.utility.ClassFinder;

public class ClassModManager<T extends Annotation> {
	private Map<Class<?>, ClassMod<T>> mods = new HashMap<>();
	
	private Class<T> annotation;
	
	private Class<?>[] objectClasses;
	
	
	public ClassModManager(Class<T> annotation) {
		this(annotation, null);
	}
	public ClassModManager(Class<T> annotation, Class<?>[] objectClasses) {
		this.annotation = annotation;
		this.objectClasses = objectClasses;
	}
	
	
	public void add(Class<?> modClass) throws ModException {
		add(new ClassMod<T>(modClass, annotation, objectClasses));
	}
	public void add(Class<?> modClass, Class<?>...objectClasses) throws ModException {
		add(new ClassMod<T>(modClass, annotation, objectClasses));
	}
	public void add(ClassMod<T> mod) throws ModException {
		if(!mod.getAnnotation().annotationType().equals(annotation)) 
			throw new ModException(mod.toString() + " has annotation " + mod.getAnnotation().annotationType() + " should have " + annotation);
		
		
		mods.put(mod.getOriginalClass(), mod);
	}
	
	public void addAllFromPath(FileHandle handle) {
		try {
			Iterable<Class<?>> i =  ClassFinder.find(handle.file().getAbsolutePath());
			
			for(Class<?> c : i) {
				if(!c.isAnnotationPresent(annotation))
					continue;
				
				add(c);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ModException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean processFields() {
		boolean allDone = true;
		
		for(ClassMod<T> mod : mods.values()) {
			boolean done = mod.processFields();
			
			if(!done)
				allDone = false;
		}
		
		return allDone;
	}
	public void executeMethods(int order) {
		for(ClassMod<T> mod : mods.values()) {
			mod.executeMethods(order);
		}
	}
	
	
	public void processAll() {
		int maxOrder = 0;
		
		for(ClassMod<T> mod : mods.values()) {
			if(mod.highestOrder() > maxOrder)
				maxOrder = mod.highestOrder();
		}
		
		for(int i = 0; i < maxOrder+1; i++) {
			for(ClassMod<T> mod : mods.values()) {
				mod.processFields();
				mod.executeMethods(i);
			}
		}
		
		for(ClassMod<T> mod : mods.values()) {
			if(!mod.processFields()) {
				Logs.log(LogType.WARNING, "could not initalize all fields for " + mod.toString());
			}
		}
	}
	
	
	public ClassMod<T> get(Class<?> modClass){
		return mods.get(modClass);
	}
	public <A> List<A> get(Class<?> modClass, Class<A> objectClass){
		List<A> list = new LinkedList<>();
		
		if(get(modClass) == null)
			return list;
		
		if(get(modClass).get(objectClass) == null)
			return list;
		
		list.addAll(get(modClass).get(objectClass));
		
		return list;
	}
}
