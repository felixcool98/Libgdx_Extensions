package com.felixcool98.lighting.occluders;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.felixcool98.aabb.shapes.Circle;

public class CircleOccluder implements Occluder {
	private Circle circle;
	
	
	public CircleOccluder(float x, float y, float radius) {
		this(new Circle(x, y, radius));
	}
	public CircleOccluder(Circle circle) {
		this.circle = circle;
	}
	
	
	@Override
	public void drawOcculuder(Batch batch, ShapeRenderer shape) {
		batch.end();
		shape.begin(ShapeType.Filled);
		shape.circle(circle.getX(), circle.getY(), circle.getRadius());
		shape.end();
		batch.begin();
	}
}
