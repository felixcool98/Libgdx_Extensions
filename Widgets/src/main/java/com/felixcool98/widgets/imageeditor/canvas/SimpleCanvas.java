package com.felixcool98.widgets.imageeditor.canvas;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.felixcool98.widgets.PixmapImage;
import com.felixcool98.widgets.Widgets;

/**
 * A simple canvas for displaying and editing an image<br>
 * contains a drawlayer and a toollayer<br>
 * <b>drawlayer</b><br>
 * displays the image represented by the canvas<br>
 * <b>toollayer</b><br>
 * for temporary display<br>
 * f.E. linedrawing preview of the line<br>
 * <br>
 * for more actual drawing look into {@link DrawableCanvas}
 * 
 * @author felixcool98
 *
 */
public class SimpleCanvas extends Table {
	/**The pixmapimage actor that represents the drawlayer**/
	private PixmapImage image;
	
	/**the scale the image needs to fit into the canvas**/
	private float initialPixelScale;
	/**the current scale the image uses**/
	private float pixelScale;
	
	/**the pixmapimage actor that represents the toollayer**/
	private PixmapImage toolLayer;
	
	/**the images x and y when centered in the canvas**/
	private float imageX, imageY;
	
	private SimpleCanvasStyle style;
	
	private FileHandle save;
	
	
	
	public SimpleCanvas() {
		this(Widgets.getSkin());
	}
	public SimpleCanvas(Skin skin) {
		this(skin.get(SimpleCanvasStyle.class));
	}
	public SimpleCanvas(SimpleCanvasStyle style) {
		super();
		
		setStyle(style);
		
		setLayoutEnabled(false);
		setTouchable(Touchable.enabled);
		
		setClip(true);
		
		init();
	}
	
	
	public void setStyle(SimpleCanvasStyle style) {
		this.style = style;
	}
	
	
	private void init() {
		createImage();
		createToolImage();
	}
	private void createImage() {
		image = new PixmapImage(1, 1) {
			ShapeRenderer shapes = new ShapeRenderer();
			
			@Override
			public void draw(Batch batch, float parentAlpha) {
				super.draw(batch, parentAlpha);
				
				batch.end();

				shapes.setProjectionMatrix(batch.getProjectionMatrix());
				shapes.setTransformMatrix(batch.getTransformMatrix());
				shapes.setColor(getStage().getDebugColor());
				
				shapes.begin(ShapeType.Line);
					shapes.rect(getX()+getImageX(), getY()+getImageY(), getWidth(), getHeight());
				shapes.end();
				
				batch.begin();
			}
		};
		image.setBackground(style.imageBackground);
		image.setScaling(Scaling.fit);
		image.setAlign(Align.center);
		
		this.add(image);
		
		imageX = image.getX();
		imageY = image.getY();
	}
	private void createToolImage() {
		toolLayer = new PixmapImage(1, 1);
		toolLayer.setTouchable(Touchable.disabled);
		
		toolLayer.setScaling(Scaling.fit);
		toolLayer.setAlign(Align.center);
		
		this.add(toolLayer);
	}
	
	
	//================================================================================================================================================
	// creating images
	//================================================================================================================================================
	/**
	 * sets the drawlayers pixmap to the given pixmap<br>
	 * you must call set width and height of the canvas before this otherwise it wont draw
	 * 
	 * @param pixmap the pixmap to set the drawlayer to
	 */
	public void setImage(Pixmap pixmap) {
		image.setPixmap(pixmap);
		toolLayer.newPixmap(image.getPixmapWidth(), image.getPixmapWidth(), image.getPixmapFormat());
		
		image.pixmapSetBlending(Blending.None);
		
		calulateInitialPixelScale();
		setPixelScale(initialPixelScale, true);
	}
	
	private void resizeImage() {
		resizeImage((float)image.getPixmapWidth()*pixelScale, (float)image.getPrefHeight()*pixelScale);
	}
	private void resizeImage(float width, float height) {
		if(image == null)
			return;
		
		image.setSize(width, height);
		image.setPosition(0, 0);
		image.layout();
		
		if(toolLayer == null)
			return;
		
		toolLayer.setSize(width, height);
		toolLayer.setPosition(0, 0);
		toolLayer.layout();
	}
	
	private void setPixelScale(float pixelScale, boolean autoResize) {
		this.pixelScale = pixelScale;
		
		if(autoResize)
			resizeImage();
	}
	
	private void calulateInitialPixelScale() {
		if(image.getPixmapWidth() < image.getPixmapHeight()) {
			float scale = getHeight() / (float)image.getPixmapHeight();
			
			initialPixelScale = scale;
		}else {
			float scale = getWidth() / (float)image.getPixmapWidth();
			
			initialPixelScale = scale;
		}
	}
	
	
	//================================================================================================================================================
	// tool layer
	//================================================================================================================================================
	public void clearToolLayer() {
		toolLayer.pixmapSetColor(Color.CLEAR);
		toolLayer.pixmapFill();
	}
	public void drawLineOnToolLayer(int x1, int y1, int x2, int y2) {
		toolLayer.pixmapSetColor(Color.WHITE);
		toolLayer.pixmapDrawLine(x1, y1, x2, y2);
	}
	
	
	//================================================================================================================================================
	// move
	//================================================================================================================================================
	public void moveImageBy(float x, float y) {
		image.moveBy(x, y);
		toolLayer.moveBy(x, y);
	}
	public void centerImage() {
		image.setPosition(imageX, imageY);
		toolLayer.setPosition(imageX, imageY);
	}
	
	
	//================================================================================================================================================
	// size
	//================================================================================================================================================
	public void setImageScale(float scale) {
		setPixelScale(scale, true);
	}
	public void multiplyImageScale(float scale) {
		setImageScale(pixelScale*scale);
	}
	public void divideImageScale(float scale) {
		setImageScale(pixelScale/scale);
	}
	public void addImageScale(float scale) {
		setPixelScale(pixelScale + scale, true);
	}
	public void setImageScaleToInitalScale() {
		setPixelScale(initialPixelScale, true);
	}
	
	
	//================================================================================================================================================
	// pixel coordinates
	//================================================================================================================================================
	protected float toPixelX(float mouseX) {
		float imageX = mouseX - image.getImageX();
		
		if(imageX < 0)
			return -1;
		
		imageX = image.getPixmapWidth() * imageX / image.getImageWidth();
		
		return imageX;
	}
	protected float toPixelY(float mouseY) {
		float imageY = mouseY - image.getImageY();
		
		if(imageY < 0)
			return -1;
		
		imageY = image.getPixmapHeight() * imageY / image.getImageHeight();
		
		return imageY;
	}
	
	
	//================================================================================================================================================
	// getter
	//================================================================================================================================================
	protected PixmapImage getImage() {
		return image;
	}
	
	
	//======================================================================
	// load / new
	//======================================================================
	public void loadImage(FileHandle handle) {
		setImage(new Pixmap(handle));
		
		save = handle;
	}
	public void newImage(int width, int height) {
		setImage(new Pixmap(width, height, Format.RGBA8888));
	}
	
	
	//======================================================================
	// save
	//======================================================================
	public void save() {
		save(save);
	}
	public void save(FileHandle handle) {
		if(handle == null)
			return;
		
		PixmapIO.writePNG(handle, image.getPixmap());
	}
	
	
	//======================================================================
	// style
	//======================================================================
	public static class SimpleCanvasStyle {
		public Drawable imageBackground;
		
		
		public boolean hasImageBackground() {
			return imageBackground != null;
		}
	}
}
