package com.felixcool98.skeleton.image;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.util.Validators;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.util.dialog.InputDialogAdapter;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTree;
import com.kotcrab.vis.ui.widget.VisValidatableTextField;

public class ImageSkeletonBoneEditor extends VisTree {
	private ImageBone bone;
	
	private VisLabel nameLabel;
	private VisTextButton parentButton;
	private Node childrenNode;
	private Node positionNode;
	
	
	public ImageSkeletonBoneEditor() {
		nameLabel = new VisLabel();
		add(new Node(nameLabel));
		
		Node parentNode = new Node(new VisLabel("parent"));
		add(parentNode);
		
		parentButton = new VisTextButton("");{
			parentButton.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if(!bone.hasParent())
						return;
					
					setBone((ImageBone) bone.getParent());
				}
			});
		}parentNode.add(new Node(parentButton));
		VisTextButton removeFromParentButton = new VisTextButton("remove from parent");{
			removeFromParentButton.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					ImageBone old = bone;
					
					if(bone.hasParent())
						setBone((ImageBone) bone.getParent());
					
					old.setParent(null);
				}
			});
		}parentNode.add(new Node(removeFromParentButton));
		
		childrenNode = new Node(new VisLabel("children"));
		add(childrenNode);
		
		positionNode = new Node(new VisLabel("position"));
		add(positionNode);
		
		setVisible(false);
	}
	
	
	public void setBone(ImageBone bone) {
		this.bone = bone;
		
		setVisible(bone != null);
		
		if(bone == null)
			return;
		
		nameLabel.setText(bone.getName());
		
		if(bone.hasParent()) {
			parentButton.setText(bone.getParent().getName());
			parentButton.setDisabled(false);
		}else {
			parentButton.setText("none");
			parentButton.setDisabled(true);
		}
		
		refreshChildrenNode();
		
		refreshPositionNode();
	}
	private void refreshChildrenNode() {
		childrenNode.removeAll();
		
		VisTextButton addChildButton = new VisTextButton("add new child");{
			addChildButton.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					Dialogs.showInputDialog(getStage(), "add new child", "name", new InputDialogAdapter() {
						@Override
						public void finished(String input) {
							bone.addChild(input);
							setBone(bone.addChild(input));
						}
					});
				}
			});
		}childrenNode.add(new Node(addChildButton));
		
		for(ImageBone child : bone.getImageChildren()) {
			VisTextButton button = new VisTextButton(child.getName());{
				button.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						setBone(child);
					}
				});
			}childrenNode.add(new Node(button));
		}
	}
	private void refreshPositionNode() {
		positionNode.removeAll();
		
		Node lengthNode = new Node(new VisLabel("length"));{
			VisValidatableTextField lengthField = new VisValidatableTextField(""+bone.getLength());{
				lengthField.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						bone.setLength(Float.parseFloat(lengthField.getText()));
					}
				});
				lengthField.addValidator(Validators.FLOATS);
			}lengthNode.add(new Node(lengthField));
		}positionNode.add(lengthNode);
		
		Node angleOnParentNode = new Node(new VisLabel("angle"));{
			VisValidatableTextField angleOnParentField = new VisValidatableTextField(""+bone.getAngleOnParent());
			VisSlider angleOnParentSlider = new VisSlider(0, 360, 1, false);
			
			angleOnParentField.addValidator(Validators.FLOATS);
			angleOnParentField.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					bone.setAngleOnParent(Float.parseFloat(angleOnParentField.getText()));
					angleOnParentSlider.setValue(bone.getAngleOnParent());
				}
			});
			angleOnParentNode.add(new Node(angleOnParentField));
			
			angleOnParentSlider.setValue(bone.getAngleOnParent());
			angleOnParentSlider.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					bone.setAngleOnParent(angleOnParentSlider.getValue());
					angleOnParentField.setText(""+bone.getAngleOnParent());
				}
			});
			angleOnParentNode.add(new Node(angleOnParentSlider));
		}positionNode.add(angleOnParentNode);
		
		refreshPositionOnParentNode();
	}
	private void refreshPositionOnParentNode() {
		Node positionOnParentNode = new Node(new VisLabel("position"));{
			if(bone.hasParent()) {
				VisValidatableTextField positionOnParentField = new VisValidatableTextField(""+bone.getPositionOnParent());
				VisSlider positionOnParentSlider = new VisSlider(0, bone.getParentLength(), bone.getParentLength()/100f, false);
				
				positionOnParentField.addValidator(Validators.FLOATS);
				positionOnParentField.addListener(new InputListener() {
					@Override
					public boolean keyDown(InputEvent event, int keycode) {
						if(keycode == Keys.ENTER) {
							bone.setPositionOnParent(Float.parseFloat(positionOnParentField.getText()));
							positionOnParentSlider.setValue(bone.getPositionOnParent());
						}
						return super.keyDown(event, keycode);
					}
				});
				positionOnParentNode.add(new Node(positionOnParentField));
				
				positionOnParentSlider.setValue(bone.getPositionOnParent());
				positionOnParentSlider.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						bone.setPositionOnParent(positionOnParentSlider.getValue());
						positionOnParentField.setText(""+bone.getPositionOnParent());
					}
				});
				positionOnParentNode.add(new Node(positionOnParentSlider));
			}
		}positionNode.add(positionOnParentNode);
	}
}
