package com.felixcool98.widgets;

public interface Loadable {
	public void load();
	public void unload();
	
	public boolean isLoaded();
}
