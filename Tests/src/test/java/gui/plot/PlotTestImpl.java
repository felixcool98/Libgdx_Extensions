package gui.plot;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.AudioRecorder;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.gui.Plot;

public class PlotTestImpl extends ImprovedGameState {
	private Plot renderer;
	
	
	@Override
	public void create() {
		renderer = new Plot();
		
		AudioRecorder recorder = Gdx.audio.newAudioRecorder(44100, true);
		short[] samples = new short[1000];
		recorder.read(samples, 0, samples.length);
		
		List<float[]> points = new LinkedList<>();
		for(int i = 0; i < samples.length; i++) {
			points.add(new float[] {i, samples[i]});
		}
		
		renderer.addPoints(points);
		
		renderer.setSize(1920, 1080);
		renderer.setPosition(0, 0);
		
		renderer.setDebug(true);
		
		renderer.setTouchable(Touchable.enabled);
		renderer.addZoomListener(1.1f);
		renderer.addDragListener();
		
		getStage().addActor(renderer);
	}
}
