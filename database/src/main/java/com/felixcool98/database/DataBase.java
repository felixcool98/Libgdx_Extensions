package com.felixcool98.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBase {
	private static boolean initialized = false;
	
	
	public static void initialize() {
		if(isInitialized())
			return;
		
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
	}
	public static boolean isInitialized() {
		return initialized;
	}
	
	
	public static Connection createConnection(String url, String user, String password) {
		initialize();
		
		try {
			return DriverManager.getConnection("jdbc:h2:"+url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	public static Connection createConnectionFromFile(String path) {
		initialize();
		
		try {
			return DriverManager.getConnection("jdbc:h2:file:"+path, "", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
