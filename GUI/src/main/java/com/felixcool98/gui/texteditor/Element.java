package com.felixcool98.gui.texteditor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

public class Element {
	private String text;
	
	private GlyphLayout layout;
	private BitmapFont font;
	private Color color;
	
	
	
	public Element(String text, BitmapFont font) {
		this(text, font, Color.WHITE);
	}
	public Element(String text, BitmapFont font, Color color) {
		this.font = font;
		this.color = color;
		this.text = text;
		
		this.layout = new GlyphLayout(font, text);
	}
	
	
	public void draw(Batch batch, float x, float y) {
		font.getCache().clear();
		font.getCache().addText(layout, x, y);
		font.getCache().tint(color);
		font.getCache().draw(batch);
	}
	public void draw(Batch batch, float x, float y, int cursor, String symbol) {
		font.getCache().clear();
		
		float cursorPos = 0;
		
		if(cursor >= 0) {
			layout.setText(font, this.text.substring(0, cursor));
			cursorPos = layout.width;
		}
			
		
		layout.setText(font, this.text);
		
		font.getCache().addText(layout, x, y);
		font.getCache().tint(color);
		font.getCache().draw(batch);
		
		font.getCache().clear();
		layout.setText(font, symbol);
		font.getCache().addText(layout, x+cursorPos, y);
		font.getCache().tint(color);
		font.getCache().draw(batch);
		
		layout.setText(font, this.text);
	}
	
	
	public float getWidth() {
		return layout.width;
	}
	public int textLength() {
		return text.length();
	}
}
