package com.felixcool98.widgets.imageeditor;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.felixcool98.widgets.imageeditor.canvas.DrawableCanvas;
import com.felixcool98.widgets.imageeditor.canvas.EditorAction;
import com.kotcrab.vis.ui.widget.VisImageButton;

public class EditorTool extends VisImageButton {
	private EditorAction action;
	private DrawableCanvas canvas;
	
	
	public EditorTool(TextureRegion region) {
		super(new TextureRegionDrawable(region));
	}
	public EditorTool(Drawable imageUp, Drawable imageDown) {
		super(imageUp, imageDown);
	}
	public EditorTool(Drawable imageUp) {
		super(imageUp);
	}
	public EditorTool(Drawable imageUp, Drawable imageDown, Drawable imageChecked) {
		super(imageUp, imageDown, imageChecked);
	}
	public EditorTool(Drawable imageUp, String tooltipText) {
		super(imageUp, tooltipText);
	}
	public EditorTool(String styleName) {
		super(styleName);
	}
	public EditorTool(VisImageButtonStyle style) {
		super(style);
	}
	
	
	public void setCanvas(DrawableCanvas canvas) {
		this.canvas = canvas;
		
		addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(EditorTool.this.action != null) {
					EditorTool.this.canvas.setEditorAction(EditorTool.this.action);
				}
			}
		});
	}
	public void setAction(EditorAction action) {
		this.action = action;
	}
	
	
	public EditorAction getAction() {
		return action;
	}
	public DrawableCanvas getCanvas() {
		return canvas;
	}
}
