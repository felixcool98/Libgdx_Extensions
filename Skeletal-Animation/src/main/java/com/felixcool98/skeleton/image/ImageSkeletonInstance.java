package com.felixcool98.skeleton.image;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.skeleton.Bone;

public class ImageSkeletonInstance {
	private ImageBone firstBone;
	
	
	
	//======================================================================
	// setter
	//======================================================================
	public ImageBone setFirstBone(String name) {
		return setFirstBone(new ImageBone(name));
	}
	public ImageBone setFirstBone(ImageBone bone) {
		firstBone = bone;
		
		return bone;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public ImageBone getBone(String name) {
		return (ImageBone) firstBone.getBone(name);
	}
	
	
	//======================================================================
	// rendering
	//======================================================================
	public void render(Batch batch, float offSetX, float offSetY) {
		List<ImageBone> bones = firstBone.getAllImageChildren();
		
		bones.sort(new Comparator<Bone>() {
			@Override
			public int compare(Bone arg0, Bone arg1) {
				if(arg0.getDepth() < arg1.getDepth())
					return 1;
				
				if(arg0.getDepth() > arg1.getDepth())
					return -1;
				
				return 0;
			}
		});
		
		for(ImageBone bone : bones) {
			bone.render(batch, offSetX, offSetY);
		}
	}
	public void render(Batch batch, float parentX, float parentY, float parentAngle) {
		firstBone.render(batch, parentX, parentY, parentAngle);
	}
	public void renderDebugLines(ShapeRenderer renderer, float parentX, float parentY, float parentAngle) {
		firstBone.renderDebugLines(renderer, parentX, parentY, parentAngle);
	}
	public void renderDebugImageOutlines(ShapeRenderer renderer, float parentX, float parentY, float parentAngle) {
		firstBone.renderDebugImageOutlines(renderer, parentX, parentY, parentAngle);
	}
	
	
	@Override
	public ImageSkeletonInstance clone() {
		ImageSkeletonInstance clone = new ImageSkeletonInstance();
		
		List<ImageBone> bones = firstBone.getAllImageChildren();
		
		while(!bones.isEmpty()) {
			List<ImageBone> remove = new LinkedList<>();
			
			for(ImageBone bone : bones) {
				if(bone.getParent() == null) {
					ImageBone newBone = clone.setFirstBone(bone.getName());
					newBone.setAll(bone);
				
					remove.add(bone);
				}else {
					ImageBone parent = clone.getBone(bone.getParent().getName());
					
					if(parent == null)
						continue;
					
					ImageBone newBone = parent.addChild(bone.getName());
					newBone.setAll(bone);
					
					remove.add(bone);
				}
			}
			
			bones.removeAll(remove);
			
			if(remove.isEmpty())
				break;
		}
		
		return clone;
	}
	
	
	public ImageBone getBoneAtPos(float x, float y, float offSetX, float offSetY) {
		return firstBone.hitAnything(x, y, offSetX, offSetY);
	}
}
