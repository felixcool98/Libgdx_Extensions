
import org.junit.Test;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.maths.parser.MathParser;

public class MathParserTest {

	@Test
	public void test() {
		String text = "a+x+b*(a*b+(z-a)+8)";
		new MathParser().parse(text);
		
		Scope object = new Scope();
		object.add("a", 1f);
		object.add("x", 2f);
		object.add("b", 3f);
		object.add("z", 4f);
		
//		float a = 1;
//		float x = 2;
//		float b = 3;
//		float z = 4;
		
//		assertTrue(function.get(null, object, null).valueEquals(a+x+b*(a*b+(z-a)+8)));
	}
}
