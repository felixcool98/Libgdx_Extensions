package com.felixcool98.gui.voicechat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.AudioDevice;
import com.badlogic.gdx.utils.Disposable;
import com.felixcool98.gui.voicechat.Recorder.RecordListener;

public class Player implements RecordListener, Disposable {
	private AudioDevice player;
	
	private float volume;
	
	
	public Player(int samples) {
		player = Gdx.audio.newAudioDevice(samples, true);
	}
	

	@Override
	public void recordPushed(short[] data) {
		player.writeSamples(data, 0, data.length);
	}
	
	
	/**
	 * sets the volume of the player
	 * 
	 * @param volume range [0,1]
	 */
	public void setVolume(float volume) {
		this.volume = volume;
		
		player.setVolume(volume);
	}
	
	
	public float getVolume() {
		return volume;
	}
	
	
	@Override
	public void dispose() {
		player.dispose();
	}
}
