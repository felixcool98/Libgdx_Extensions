package com.felixcool98.modding.directory;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.files.FileHandle;
import com.felixcool98.resources.Directory;

public class ModDirectoryManager {
	private ModDirectoryProcessor processor;
	
	private Directory directory;
	
	private Map<String, ModDirectory> mods = new HashMap<>();
	
	
	public void setProcessor(ModDirectoryProcessor processor) {
		this.processor = processor;
	}
	public void setDirectory(Directory directory) {
		this.directory = directory;
	}
	
	
	public void start() {
		for(FileHandle handle : directory.getHandle().list()) {
			if(!handle.isDirectory())
				continue;
			
			mods.put(handle.name(), new ModDirectory(handle));
		}
		
		for(ModDirectory directory : mods.values()) {
			processor.process(this, directory);
		}
	}
	
	
	public ModDirectory getMod(String name) {
		return mods.get(name);
	}
}
