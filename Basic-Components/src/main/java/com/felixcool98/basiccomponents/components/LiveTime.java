package com.felixcool98.basiccomponents.components;

import com.felixcool98.basiccomponents.data.LiveTimeData;
import com.felixcool98.components.HasData;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.ComponentManager;

public class LiveTime implements HasData.CanCreateData<LiveTimeData> {
	private float liveTime;
	
	
	public LiveTime(float liveTime) {
		this.liveTime = liveTime;
	}
	
	
	public float getLiveTime() {
		return liveTime;
	}
	
	
	@Override
	public LiveTimeData create(ComponentManager manager, CreationData data) {
		return new LiveTimeData(this);
	}
}
