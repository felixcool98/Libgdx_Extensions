package com.felixcool98.dialogs;

public class SimpleDialog extends Dialog {
	private String text;
	
	
	public SimpleDialog(String text) {
		super();
		
		this.text = text;
	}
	public SimpleDialog(Character left, Character right, String text) {
		super(left, right);
		
		this.text = text;
	}
	public SimpleDialog(Character left, String leftImage,Character right, String rightImage, String text) {
		super(left, leftImage, right, rightImage);
		
		this.text = text;
	}
	
	
	@Override
	public String getText() {
		return text;
	}
}
