package com.felixcool98.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

public class PixmapUtils {
	//================================================================================================================================================
	// pixmap helper method
	//================================================================================================================================================
	public static Pixmap fillPixMapWithPattern(Pixmap pixMap, Pixmap pattern, boolean disposePattern) {
		for(int x = 0; x < pixMap.getWidth()/pattern.getWidth(); x++) {
			for(int y = 0; y < pixMap.getHeight()/pattern.getHeight(); y++) {
				pixMap.drawPixmap(pattern, x*pattern.getWidth(), y*pattern.getHeight());
			}
		}
		
		if(disposePattern)
			pattern.dispose();
		
		return pixMap;
	}
	public static Pixmap fillPixMapWithPattern(Pixmap pixMap, Pixmap pattern) {
		return fillPixMapWithPattern(pixMap, pattern, true);
	}
	
	public static Pixmap createBackgroundPattern(int size, Color firstColor, Color secondColor) {
		Pixmap pattern = new Pixmap(size, size, Format.RGBA8888);
		
		pattern.setColor(firstColor);
		pattern.fill();
		
		pattern.setColor(secondColor);
		
		for(int i = 0; i < size / 4; i++) {
			pattern.drawRectangle(i, i, size/2-2*i, size/2-2*i);
		}
		for(int i = 0; i < size / 4; i++) {
			pattern.drawRectangle(size/2+i, size/2+i, size/2-2*i, size/2-2*i);
		}
		
		return pattern;
	}
	
	public static Pixmap creatFlippedPixMap(Pixmap pixMap) {
		Pixmap save = new Pixmap(pixMap.getWidth(), pixMap.getHeight(), pixMap.getFormat());
		
		for(int x = 0; x < pixMap.getWidth(); x++) {
			for(int y = 0; y < pixMap.getHeight(); y++) {
				int pixel = pixMap.getPixel(x, y);
				
				save.drawPixel(x, save.getHeight()-1-y, pixel);
			}
		}
		
		return save;
	}
	
	public static Pixmap copyPixmap(Pixmap pixmap) {
		Pixmap copy = new Pixmap(pixmap.getWidth(), pixmap.getHeight(), pixmap.getFormat());
		
		copy.drawPixmap(pixmap, 0, 0);
		
		return copy;
	}
	
	public static void fillPixmap(Pixmap pixmap, int x, int y, Color color){
		int searchColor = pixmap.getPixel(x, y);
		
		pixmap.setColor(color);
		
		fillPixmap(pixmap, pixmap.getWidth(), pixmap.getHeight(), x, y, searchColor, -1);
	}
	private static void fillPixmap(Pixmap pixmap, int width, int height, int x, int y, int color, int dir){
		if(x < 0 || y < 0 || x > width || y > height) {
			return;
		}
		
		if(pixmap.getPixel(x, y) == color) {
			pixmap.drawPixel(x, y);
			
			if(dir != 1)
				fillPixmap(pixmap, width, height, x-1, y, color, 0);
			if(dir != 0)
				fillPixmap(pixmap, width, height, x+1, y, color, 1);
			if(dir != 3)
				fillPixmap(pixmap, width, height, x, y-1, color, 2);
			if(dir != 2)
				fillPixmap(pixmap, width, height, x, y+1, color, 3);
		}
	}
	
	
	//======================================================================
	// scale
	//======================================================================
	public static Pixmap upscale(Pixmap old, int scale) {
		Pixmap pixmap = new Pixmap(old.getWidth()*scale, old.getHeight()*scale, old.getFormat());
		
		for(int i = 0; i < old.getWidth(); i++) {
			for(int j = 0; j < old.getHeight(); j++) {
				int pixel = old.getPixel(i, j);
				
				pixmap.setColor(pixel);
				pixmap.fillRectangle(i*scale, j*scale, scale, scale);
			}
		}
		
		return pixmap;
	}
}
