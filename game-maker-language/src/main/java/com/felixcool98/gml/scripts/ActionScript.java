package com.felixcool98.gml.scripts;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.actions.Action;
import com.felixcool98.gml.objects.GMLObject;

public class ActionScript implements Script {
private String name;
	
	private String[] arguments;
	
	private List<Action> actions = new LinkedList<>();
	
	
	void addAction(Action action) {
		actions.add(action);
	}
	
	void setArguments(String[] arguments) {
		for(int i = 0; i < arguments.length; i++) {
			arguments[i] = arguments[i].trim();
		}
		
		this.arguments = arguments;
	}
	void setName(String name) {
		this.name = name;
	}
	
	
	public String getName() {
		return name;
	}
	public String[] getArguments() {
		return arguments;
	}
	
	public String getArgumentsAsString() {
		String str = "";
		
		for(String arg : arguments) {
			str += arg+", ";
		}
		
		if(str.contains(", ")) {
			str = str.substring(0, str.lastIndexOf(", "));
		}
		
		return str;
	}
	
	
	@Override
	public GMLObject execute(Scope global, Scope object, GMLObject... objects) {
		Scope local = new Scope();
		
		for(int i = 0; i < arguments.length; i++) {
			local.add("argument" + i, new GMLObject(arguments[i]));
		}
	
		for(Action action : actions) {
			action.execute(global, object, local);
		}
		
		return null;
	}
	
	
	@Override
	public String toString() {
		return getName() + "(" + getArgumentsAsString()+")";
	}
}
