package com.felixcool98.dialogs.options;

import com.felixcool98.dialogs.Dialog;
import com.felixcool98.dialogs.gui.DialogDisplay;

public class OptionOpenDialog extends Option {
	private Dialog dialog;
	
	
	public OptionOpenDialog(String text, Dialog dialog) {
		super(text);
		
		this.dialog = dialog;
	}

	
	@Override
	public void clicked(DialogDisplay display) {
		display.setDialog(dialog);
		callListeners();
	}
	
	
	public Dialog getDialog() {
		return dialog;
	}
}
