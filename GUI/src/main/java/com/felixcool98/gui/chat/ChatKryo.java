package com.felixcool98.gui.chat;

import java.util.Date;

import com.esotericsoftware.kryo.Kryo;
import com.felixcool98.gui.chat.Message.ConnectionMessage;
import com.felixcool98.gui.chat.Message.NameTextMessage;

public class ChatKryo {
	public static void prepareKry(Kryo kryo) {
		kryo.register(ConnectionMessage.class);
		kryo.register(Date.class);
		kryo.register(NameTextMessage.class);
	}
}
