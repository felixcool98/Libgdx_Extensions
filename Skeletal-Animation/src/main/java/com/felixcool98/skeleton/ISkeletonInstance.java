package com.felixcool98.skeleton;

public interface ISkeletonInstance<T extends Bone> extends Cloneable {
	public T setFirstBone(String name);
	public T setFirstBone(T bone);
	
	public T getBone(String name);
	
	public ISkeletonInstance<T> clone() throws CloneNotSupportedException;
}
