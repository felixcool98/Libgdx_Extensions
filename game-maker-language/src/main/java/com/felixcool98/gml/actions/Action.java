package com.felixcool98.gml.actions;

import com.felixcool98.gml.Scope;

public interface Action {
	public void execute(Scope global, Scope object, Scope local);
}
