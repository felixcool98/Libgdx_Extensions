package com.felixcool98.widgets.listeners;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

/**
 * simple listener for makeing actors draggable<br>
 * if the actor the listener is added to is dragged the given actor will move 
 * 
 * @author felixcool98
 *
 */
public class Draggable extends DragListener {
	private Actor actor;
	
	private float startX, startY;
	
	
	public Draggable(Actor actor) {
		this.actor = actor;
	}
	
	
	@Override
	public void dragStart(InputEvent event, float x, float y, int pointer) {
		startX = x;
		startY = y;
	}
	@Override
	public void drag(InputEvent event, float x, float y, int pointer) {
		x -= startX;
		y -= startY;
		
		actor.moveBy(x, y);
	}
}
