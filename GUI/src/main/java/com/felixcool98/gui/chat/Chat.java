package com.felixcool98.gui.chat;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.gui.chat.ChatClient.MessageReceiver;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;

public class Chat extends Table implements MessageReceiver {
	private VisScrollPane pane;
	private Table chatTable;
	private String name;
	
	private ChatClient client;
	
	private boolean autocroll = true;
	
	
	public Chat(float chatWidth, float chatHeight, String name) {
		this.name = name;
		
		chatTable = new Table();
		pane = new VisScrollPane(chatTable);{
			pane.setScrollingDisabled(true, false);
			pane.setOverscroll(false, false);
			pane.setScrollbarsOnTop(false);
			pane.setFadeScrollBars(false);
		}this.add(pane).size(chatWidth, chatHeight);
		
		this.row();
		
		Table table = new Table();{
			VisTextField textField = new VisTextField();
			textField.setAlignment(Align.left);
			table.add(textField).align(Align.left);
			
			textField.addListener(new InputListener() {
				@Override
				public boolean keyDown(InputEvent event, int keycode) {
					if(keycode == Keys.ENTER) {
						String text = textField.getText();
						textField.setText("");
						
						sendMessage(text);
					}
					
					return super.keyDown(event, keycode);
				}
			});
			VisTextButton sendButton = new VisTextButton("send");{
				sendButton.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						String text = textField.getText();
						textField.setText("");
						
						sendMessage(text);
					}
				});
			}table.add(sendButton).pad(5);
		}this.add(table);
	}
	
	
	//======================================================================
	// adding things to the pane
	//======================================================================
	public void addToPane(Label label) {
		label.setAlignment(Align.topLeft);
		chatTable.add(label).align(Align.topLeft).expandX().fillX();
		chatTable.row();
		chatTable.validate();
		
		if(autocroll)
			pane.scrollTo(0, 0, 0, 0);
	}
	public void addMessage(Message message) {
		if(message == null || message.toString().length() == 0)
			return;
		
		if(message instanceof Message.NameTextMessage) {
			VisLabel label = new VisLabel(message.toString());
			label.setWrap(true);
			label.setAlignment(Align.topLeft);
			chatTable.add(label).align(Align.topLeft).expandX().fillX();
			chatTable.row();
			chatTable.pack();
		}else if(message instanceof Message.ConnectionMessage) {
			VisLabel label = new VisLabel(message.toString());
			label.setWrap(true);
			label.setAlignment(Align.topLeft);
			chatTable.add(label).align(Align.topLeft).expandX().fillX();
			chatTable.row();
			chatTable.pack();
		}
	}
	public void sendMessage(String message) {
		client.sendMessage(new Message.NameTextMessage(name, message));
	}
	@Override
	public void messageReceived(Message message) {
		addMessage(message);
	}
	public void clearMessages() {
		chatTable.clearChildren();
	}
	
	public void setChatName(String name) {
		this.name = name;
	}
	public String getChatName() {
		return name;
	}
	
	
	public void setClient(ChatClient client) {
		this.client = client;
	}
}
