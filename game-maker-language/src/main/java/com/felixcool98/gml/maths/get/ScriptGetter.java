package com.felixcool98.gml.maths.get;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.maths.MathFunction;
import com.felixcool98.gml.maths.parser.MathParser;
import com.felixcool98.gml.objects.GMLObject;

public class ScriptGetter implements MathFunction {
	private String name;
	private List<MathFunction> arguments = new LinkedList<>();
	
	
	public ScriptGetter(String name, String... arguments) {
		this.name = name;
		
		for(String argument : arguments) {
			MathFunction function = new MathParser().parse(argument.trim());
			
			if(function != null)
				this.arguments.add(function);
		}
	}
	
	
	@Override
	public GMLObject get(Scope global, Scope object, Scope local) {
		GMLObject[] objects = new GMLObject[arguments.size()];
		
		for(int i = 0; i < arguments.size(); i++) {
			objects[i] = arguments.get(i).get(global, object, local);
		}
		
		return global.getScript(name).execute(global, object, objects);
	}
}
