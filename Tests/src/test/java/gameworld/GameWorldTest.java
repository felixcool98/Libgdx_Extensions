package gameworld;

import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.events.EventListener;
import com.felixcool98.gameworld.GameWorld;
import com.felixcool98.gameworld.GameWorldObject;
import com.felixcool98.gameworld.Step;
import com.felixcool98.gameworld.events.BeforeCreationEvent;

public class GameWorldTest {
	public static void main(String... args) {
		GameWorld world = new GameWorld();
		
		world.create(new TestObject(new Rectangle(1, 1)).setSpeed(1, 1));
		
		while(true) {
			world.step(1);
		}
	}
	
	
	private static class TestObject extends GameWorldObject{
		private float speedX, speedY;
		
		
		public TestObject(Shape shape) {
			super(shape);
		}
		
		
		public TestObject setSpeed(float x, float y) {
			speedX = x;
			speedY = y;
			
			return this;
		}
		
		
		@Step(group = "move")
		public void moveStep(float delta) {
			getShape().move(speedX * delta, speedY * delta);
			System.out.println(getShape());
		}
		
		
		@EventListener
		public void beforeCreate(BeforeCreationEvent event) {
			System.out.println("before");
		}
	}
}
