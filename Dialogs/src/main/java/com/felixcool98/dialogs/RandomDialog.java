package com.felixcool98.dialogs;

import java.util.List;

import com.felixcool98.utility.random.Random;

public class RandomDialog extends Dialog {
	private List<String> texts;
	
	
	public RandomDialog(List<String> texts) {
		super();
		
		this.texts = texts;
	}
	public RandomDialog(Character left, Character right, List<String> texts) {
		super(left, right);
		
		this.texts = texts;
	}
	public RandomDialog(Character left, String leftImage,Character right, String rightImage, List<String> texts) {
		super(left, leftImage, right, rightImage);
		
		this.texts = texts;
	}
	
	
	@Override
	public String getText() {
		return Random.INSTANCE.randomObjectFromList(texts);
	}
}
