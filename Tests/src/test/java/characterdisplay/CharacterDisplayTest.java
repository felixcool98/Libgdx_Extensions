package characterdisplay;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.felixcool98.game.Game;
import com.felixcool98.game.GameState;
import com.felixcool98.game.GameStateManager;

public class CharacterDisplayTest {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("CharacterDisplayTest");
		config.setWindowSizeLimits(1920, 1080, 1920, 1080);
		
		new Lwjgl3Application(new Game() {
			@Override
			protected GameState registerGameStates(GameStateManager manager) {
				return new CharacterDisplayTestImpl();
			}
		}, config);
	}
}
