package com.felixcool98.widgets.html;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;

public class Paragraph implements IElement {
	private Skin skin;
	
	private Table table;
	
	private String rest;
	
	private float padding = 10;
	
	
	public Paragraph(String text, Skin skin) {
		this.skin = skin;
		
		process(text);
	}
	
	
	private void process(String text) {
		table = new ElementTable();
		table.padTop(padding).padBottom(padding);
		
		String element = text.substring(0, text.indexOf(">")+1);
		
		text = text.replaceFirst(element, "");
		
		String paragraph;
		
		if(text.contains("</p>")) {
			paragraph = text.substring(0, text.indexOf("</p>"));
			
			text = text.replaceFirst("</p>", "");
		}
		else
			paragraph = text;
		
		Label label = new VisLabel(paragraph, skin.get(LabelStyle.class));
		table.add(label).align(Align.topLeft);
		
		Attributes.process(element, table, label);
		
		rest = text.replaceFirst(paragraph, "");
	}
	
	
	public String getRest() {
		return rest;
	}
	
	
	public Table getTable() {
		return table;
	}
	
	
	public static class ParagraphBuilder implements IElementBuilder<Paragraph> {
		@Override
		public Paragraph createElement(String text, Skin skin) {
			return new Paragraph(text, skin);
		}
		
		@Override
		public boolean valid(String text) {
			return text.startsWith("<p>") || text.startsWith("<p ");
		}
	}
}
