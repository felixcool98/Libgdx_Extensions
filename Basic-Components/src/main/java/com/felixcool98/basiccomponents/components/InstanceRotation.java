package com.felixcool98.basiccomponents.components;

import com.felixcool98.basiccomponents.data.RotationData;
import com.felixcool98.components.HasData;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.ComponentManager;


/**
 * 
 * CreationData needs to contain: <br>
 * rotation : float<br>
 * 
 * @author felixcool98
 *
 */
public class InstanceRotation implements HasData.CanCreateData<RotationData> {
	@Override
	public RotationData create(ComponentManager manager, CreationData data) {
		RotationData rotation = new RotationData();
		
		if(data.containsKey("rotation"))
			rotation.setRotation(data.getFloat("rotation"));
		
		return rotation;
	}

}
