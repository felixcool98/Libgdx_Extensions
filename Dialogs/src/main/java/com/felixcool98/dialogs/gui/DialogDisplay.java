package com.felixcool98.dialogs.gui;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.dialogs.Dialog;
import com.felixcool98.dialogs.gui.CharacterDisplay.CharacterDisplayStyle;
import com.felixcool98.dialogs.options.Option;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTextButton;

public class DialogDisplay extends Table {
	private Table displays;
	private CharacterDisplay left;
	private CharacterDisplay right;
	
	private float textHeight;
	private VisScrollPane textScrollPane;
	private VisLabel textLabel;
	
	private float buttonPaneHeight;
	private VisScrollPane buttonScrollPane;
	private Table buttonTable;
	
	private boolean needsRebuild = true;
	
	
	public DialogDisplay(DialogDisplayStyle style) {
		this();
		
		setStyle(style);
	}
	public DialogDisplay() {
		displays = new Table();
		
		left = new CharacterDisplay();{
			left.align(Align.left);
		}
		right = new CharacterDisplay();{
			right.align(Align.right);
		}
		
		textLabel = new VisLabel();{
			textLabel.setWrap(true);
		}
		
		textScrollPane = new VisScrollPane(textLabel);{
			textScrollPane.setScrollingDisabled(true, false);
			textScrollPane.setOverscroll(false, false);
			textScrollPane.setScrollbarsOnTop(false);
			textScrollPane.setFadeScrollBars(false);
		}
		
		buttonTable = new Table();
		buttonScrollPane = new VisScrollPane(buttonTable);{
			buttonScrollPane.setScrollingDisabled(true, false);
			buttonScrollPane.setOverscroll(false, false);
			buttonScrollPane.setScrollbarsOnTop(false);
			buttonScrollPane.setFadeScrollBars(false);
		}
	}
	
	
	public void rebuild() {
		this.clearChildren();
		
		displays.clearChildren();
		displays.add(left).width(getWidth()/2f);
		displays.add(right).width(getWidth()/2f);
		
		
		this.add(displays).width(getWidth());
		
		this.row();
		
		this.add(textScrollPane).align(Align.topLeft).width(getWidth()).height(textHeight);
		
		this.row();
		
		this.add(buttonScrollPane).width(getWidth()).height(buttonPaneHeight);

		needsRebuild = false;
		
		pack();
	}
	
	
	public void setButtonPaneHeight(float height) {
		this.buttonPaneHeight = height;
		
		invalidate();
		
		needsRebuild = true;
	}
	public void setTextHeight(float height) {
		this.textHeight = height;
		
		invalidate();
		
		needsRebuild = true;
	}
	public void setCharacterDisplayStyle(CharacterDisplayStyle style) {
		left.setStyle(style);
		right.setStyle(style);
		
		invalidate();
		
		needsRebuild = true;
	}
	
	
	public void setStyle(DialogDisplayStyle style) {
		setWidth(style.width);
		setTextHeight(style.textHeight);
		setCharacterDisplayStyle(style.characterDisplayStyle);
		setButtonPaneHeight(style.buttonPaneHeight);
		
		rebuild();
	}
	
	
	public void setDialog(Dialog dialog) {
		if(dialog == null) {
			left.setCharacter(null, "");
			right.setCharacter(null, "");
			
			textLabel.setText("");
			
			buttonTable.clearChildren();
			
			return;
		}
		
		left.setCharacter(dialog.getLeft(), dialog.getLeftImage());
		if(dialog.leftSpeaks()) {
			left.setColor(1, 1, 1, 1);
		}else {
			left.setColor(0.5f, 0.5f, 0.5f, 1);
		}
		right.setCharacter(dialog.getRight(), dialog.getRightImage());
		if(dialog.rightSpeaks()) {
			right.setColor(1, 1, 1, 1);
		}else {
			right.setColor(0.5f, 0.5f, 0.5f, 1);
		}
		
		textLabel.setText(dialog.getText());
		
		constructButtons(dialog);
		
		pack();
	}
	
	
	private void constructButtons(Dialog dialog) {
		List<VisTextButton> buttons = new LinkedList<>();
		
		for(Option option : dialog.getOptions()) {
			if(option.hidden())
				continue;
			
			VisTextButton button = new VisTextButton(option.getText(), new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					option.clicked(DialogDisplay.this);
				}
			});
			
			button.setDisabled(!option.canBeClicked());
			
			buttons.add(button);
		}
		
		float width = 0;
		
		buttonTable.clear();
		
		for(VisTextButton button : buttons) {
			if(width + button.getWidth() > getWidth()) {
				width = 0;
				buttonTable.row();
			}
			
			width += button.getWidth();
			buttonTable.add(button);
		}
		
		buttonTable.layout();
		
		pack();
	}
	
	
	@Override
	public void validate() {
		super.validate();
		
		if(needsRebuild)
			rebuild();
	}
	@Override
	public void setWidth(float width) {
		super.setWidth(width);
		
		invalidate();
		
		needsRebuild = true;
	}
	
	
	public static class DialogDisplayStyle {
		public float width;
		public float textHeight;
		public float buttonPaneHeight;
		
		public CharacterDisplayStyle characterDisplayStyle;
	}
}
