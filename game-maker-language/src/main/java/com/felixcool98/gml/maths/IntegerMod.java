package com.felixcool98.gml.maths;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.objects.GMLObject;

public class IntegerMod extends MathFunctionAdapter {
	public IntegerMod(MathFunction f1, MathFunction f2) {
		super(f1, f2);
	}
	
	
	@Override
	public GMLObject get(Scope global, Scope object, Scope local) {
		return f1.get(global, object, local).intMod(f2.get(global, object, local));
	}

}
