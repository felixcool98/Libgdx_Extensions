package com.felixcool98.basiccomponents.components;

import com.felixcool98.basiccomponents.data.PositionData;
import com.felixcool98.components.HasData;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.managers.ComponentManager;

/**
 * 
 * CreationData needs to contain: <br>
 * x : float<br>
 * y : float<br>
 * 
 * @author felixcool98
 *
 */
public class Position implements HasData.CanCreateData<PositionData> {
	@Override
	public PositionData create(ComponentManager manager, CreationData data) {
		PositionData position = new PositionData();
		
		position.setPosition(data.getFloat("x"), data.getFloat("y"));
		
		return position;
	}
}
