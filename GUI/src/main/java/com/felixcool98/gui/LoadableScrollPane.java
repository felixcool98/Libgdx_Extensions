package com.felixcool98.gui;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.widgets.Loadable;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.bruteforce.BruteForceWorld;
import com.felixcool98.worlds.objects.World2DObject;
import com.kotcrab.vis.ui.widget.VisScrollPane;

/**
 * Scroll pane that automatically calls load and unload on any underlying loadable actors<br>
 * layout needs to be called if the underlying actors change
 * 
 * @author felixcool98
 *
 */
public class LoadableScrollPane extends VisScrollPane {
	private World2D world = new BruteForceWorld();
	
	private Rectangle last = new Rectangle(0, 0, 1, 1);
	
	private Map<Actor, World2DObject> actors = new HashMap<>();
	
	
	public LoadableScrollPane(Actor widget, String styleName) {
		super(widget, styleName);
		
		init();
	}
	public LoadableScrollPane(Actor widget, ScrollPaneStyle style) {
		super(widget, style);
		
		init();
	}
	public LoadableScrollPane(Actor widget) {
		super(widget);
		
		init();
	}
	
	
	private void init() {
		if(getActor() == null || !(getActor() instanceof Group))
			return;
		
		if(!((Group) getActor()).hasChildren())
			return;
		
		for(Actor child : ((Group) getActor()).getChildren()) {
			register(child);
		}
		
		addListener(new DragListener() {
			@Override
			public void drag(InputEvent event, float x, float y, int pointer) {
				updateLoaded();
			}
		});
	}
	
	
	@Override
	protected void scrollX(float pixelsX) {
		super.scrollX(pixelsX);
		
		updateLoaded();
	}
	@Override
	protected void scrollY(float pixelsY) {
		super.scrollY(pixelsY);
		
		updateLoaded();
	}
	
	private void updateLoaded() {
		if(getActor() == null || !(getActor() instanceof Group))
			return;
		
		if(!((Group) getActor()).hasChildren())
			return;
		
		for(World2DObject object : actors.values()) {
			update(object);
		}
		
		Rectangle scrollRect = new Rectangle(getScrollX(), getActor().getHeight()-getScrollY()-getHeight(), getWidth(), getHeight());
		
		for(World2DObject object : world.getIntersections(last)) {
			if(object.getShape().overlaps(scrollRect))
				continue;
			
			if(object.getUserObject() instanceof Loadable)
				((Loadable) object.getUserObject()).unload();
		}
		
		for(World2DObject object : world.getIntersections(scrollRect)) {
			if(object.getUserObject() instanceof Loadable)
				((Loadable) object.getUserObject()).load();
		}
		
		last = scrollRect;
	}
	
	@Override
	public void layout() {
		super.layout();
		
		if(getActor() == null || !(getActor() instanceof Group))
			return;
		
		if(!((Group) getActor()).hasChildren())
			return;
		
		for(Actor child : ((Group) getActor()).getChildren()) {
			if(actors.containsKey(child))
				continue;
			else
				register(child);
		}
		
		List<Actor> remove = new LinkedList<>();
		
		for(Actor actor : actors.keySet()) {
			if(!((Group) getActor()).getChildren().contains(actor, false))
				remove.add(actor);
		}
		
		for(Actor actor : remove) {
			actors.remove(actor);
		}
		
		updateLoaded();
	}
	
	private void register(Actor actor) {
		World2DObject object = world.create(new Rectangle(0, 0, 1, 1));
		
		object.setUserObject(actor);
		
		actors.put(actor, object);
		
		update(object);
	}
	
	private void update(World2DObject object) {
		Actor actor = (Actor) object.getUserObject();
		
		Rectangle rect = (Rectangle) object.getShape();
		
		rect.set(actor.getX(), actor.getY(),
				actor.getWidth(), actor.getHeight());
	}
}
