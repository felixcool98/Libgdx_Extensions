package com.felixcool98.dialogs.options;

import com.felixcool98.dialogs.gui.DialogDisplay;

public class OptionEndDialog extends Option {
	public OptionEndDialog(String text) {
		super(text);
	}

	
	@Override
	public void clicked(DialogDisplay display) {
		display.setDialog(null);
		callListeners();
	}
}
