package com.felixcool98.net;

import java.util.LinkedList;
import java.util.List;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.felixcool98.utility.threads.ThreadUtils;

public abstract class Server implements Net {
	private com.esotericsoftware.kryonet.Server server;
	
	private boolean running = false;
	
	private List<StartStopListener> startStopListeners = new LinkedList<>();
	
	
	public Server(int bufferSize) {
		this(bufferSize, bufferSize);
	}
	/**
	 * 
	 * @param writeBuffer default 16384 
	 * @param objectBuffer default 2048
	 */
	public Server(int writeBuffer, int objectBuffer) {
		server = new com.esotericsoftware.kryonet.Server(writeBuffer, objectBuffer);
		init();
	}
	public Server() {
		server = new com.esotericsoftware.kryonet.Server();
		init();
	}
	
	private void init() {
		registerClasses(server.getKryo());
		server.addListener(new Listener() {
			@Override
			public void received(Connection connection, Object object) {
				ThreadUtils.runSynchronous(()-> {
					Server.this.received(connection, object);
				});
			}
		});
	}
	
	
	public void start(int port) {
		if(isRunning())
			stop();
		
		try {
			server.start();
			server.bind(port);
			
			for(StartStopListener listener : startStopListeners) {
				listener.serverStarted();
			}
			
			running = true;
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void stop() {
		if(!isRunning())
			return;
		
		for(StartStopListener listener : startStopListeners) {
			listener.serverStopped();
		}
		
		server.stop();
		
		running = false;
	}
	
	
	public abstract void received(Connection connection, Object object);
	
	
	public com.esotericsoftware.kryonet.Server getServer(){
		return server;
	}
	public boolean isRunning() {
		return running;
	}
	
	
	//======================================================================
	// listeners
	//======================================================================
	public void addListener(StartStopListener listener) {
		startStopListeners.add(listener);
	}
	
	
	public static interface StartStopListener {
		public void serverStarted();
		public void serverStopped();
	}
}
