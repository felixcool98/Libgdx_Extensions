package com.felixcool98.basiccomponents.data;

import com.felixcool98.basiccomponents.components.LiveTime;
import com.felixcool98.components.HasData;
import com.felixcool98.data.Data;

public class LiveTimeData implements Data {
	private LiveTime time;
	private float liveTime;
	
	
	public LiveTimeData(LiveTime liveTime) {
		this.time = liveTime;
	}
	
	
	public void update(float delta) {
		liveTime += delta;
	}
	
	
	public float getTime() {
		return liveTime;
	}
	
	
	public boolean isDead() {
		return time.getLiveTime() < liveTime;
	}
	
	
	@Override
	public Class<? extends HasData<?>> getDataClass() {
		return LiveTime.class;
	}
}
