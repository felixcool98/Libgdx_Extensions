package com.felixcool98.skeleton;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.math.VectorMath;
import com.felixcool98.skeleton.IBone.BoneImpl;

public class Bone extends BoneImpl<Bone> {
	private int depth;
	
	
	public Bone(String name) {
		super(name);
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	public void setAll(Bone bone) {
		depth = bone.depth;
		
		setAngleOnParent(bone.getAngleOnParent());
		setLength(bone.getLength());
		setPositionOnParent(bone.getPositionOnParent());
	}
	
	
	public void setData(float angleOnParent, float length, float position) {
		setAngleOnParent(angleOnParent);
		setLength(length);
		setPositionOnParent(position);
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public float getAngle() {
		if(getParent() == null)
			return getAngle(0);
		
		return getAngle(getParent().getAngle());
	}
	public float getAngle(float parentAngle) {
		return getAngleOnParent() + parentAngle;
	}
	
	
	public float getX() {
		return getX(getParentX(), getParentAngle());
	}
	public float getX(float parentX, float parentAngle) {
		return parentX + VectorMath.vectorX(parentAngle, getPositionOnParent());
	}
	public float getY() {
		return getY(getParentY(), getParentAngle());
	}
	public float getY(float parentY, float parentAngle) {
		return parentY + VectorMath.vectorY(parentAngle, getPositionOnParent());
	}
	public float getEndX(float parentX, float parentAngle) {
		return getX(parentX, parentAngle) + VectorMath.vectorX(getAngle(parentAngle), getLength());
	}
	public float getEndY(float parentY, float parentAngle) {
		return getY(parentY, parentAngle) + VectorMath.vectorY(getAngle(parentAngle), getLength());
	}
	
	
	//======================================================================
	// render
	//======================================================================
	
	public void renderDebugLines(ShapeRenderer renderer, float parentX, float parentY, float parentAngle) {
		renderer.setAutoShapeType(true);
		
		for(Bone child : getChildren()) {
			child.renderDebugLines(renderer, getX(parentX, parentAngle), getY(parentY, parentAngle), getAngle(parentAngle));
		}
		
		renderer.begin();{
			renderer.line(getX(parentX, parentAngle), getY(parentY, parentAngle), getEndX(parentX, parentAngle), getEndY(parentY, parentAngle));
		}renderer.end();
	}
	
	
	//======================================================================
	// children and parents
	//======================================================================
	public Bone addChild(String name) {
		return addChild(new Bone(name));
	}
	
	
	//======================================================================
	// drawing
	//======================================================================
	
	
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	//getter
	public int getDepth() {
		return depth;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	
	//parent
	protected float getParentX() {
		if(!hasParent())
			return 0;
		
		return getParent().getX();
	}
	protected float getParentY() {
		if(!hasParent())
			return 0;
		
		return getParent().getY();
	}
	
	protected float getParentAngle() {
		if(!hasParent())
			return 0;
		
		return getParent().getAngle();
	}
}
