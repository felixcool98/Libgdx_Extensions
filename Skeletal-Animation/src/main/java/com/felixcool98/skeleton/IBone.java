package com.felixcool98.skeleton;

import java.util.LinkedList;
import java.util.List;

public interface IBone<T extends IBone<T>> {
	public void setName(String name);
	public String getName();
	
	public T getBone(String name);
	
	public List<T> getAllBones();
	
	public void setLength(float length);
	public float getLength();
	
	public void setAngleOnParent(float angle);
	public float getAngleOnParent();
	
	public void setPositionOnParent(float position);
	public float getPositionOnParent();
	
	
	//======================================================================
	// children
	//======================================================================
	
	public T addChild(T child);
	public T removeChild(T child);
	
	//checks
	public boolean hasChild(String name);
	public boolean hasChild(T name);
	
	//getter
	public T getChild(String name);
	
	public List<T> getChildren();
	
	
	//======================================================================
	// parent
	//======================================================================
	
	//setter
	public T setParent(T parent);
	
	//getter
	public T getParent();
	
	public float getParentLength();
	
	//checks
	public boolean hasParent();
	
	
	public static class BoneImpl<T extends IBone<T>> implements IBone<T> {
		private String name;
		
		private List<T> children = new LinkedList<>();
		
		private T parent;
		
		private float length;
		private float angleOnParent;
		
		private float positionOnParent;
		
		
		public BoneImpl(String name) {
			this.name = name;
		}
		
		
		@Override
		public void setName(String name) {
			this.name = name;
		}
		@Override
		public String getName() {
			return name;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public T getBone(String name) {
			if(getName().equals(name))
				return (T) this;
			
			for(T child : children) {
				T searched = child.getBone(name);
				
				if(searched != null)
					return searched;
			}
			
			return null;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public List<T> getAllBones() {
			List<T> all = new LinkedList<>();
			
			all.add((T) this);
			
			if(children.isEmpty())
				return all;
			
			for(T child : children) {
				all.addAll(child.getAllBones());
			}
			
			return all;
		}
		
		@Override
		public void setLength(float length) {
			this.length = length;
		}
		@Override
		public float getLength() {
			return length;
		}
		
		@Override
		public void setAngleOnParent(float angle) {
			this.angleOnParent = angle;
		}
		@Override
		public float getAngleOnParent() {
			return angleOnParent;
		}
		
		@Override
		public void setPositionOnParent(float position) {
			this.positionOnParent = position;
		}
		@Override
		public float getPositionOnParent() {
			return positionOnParent;
		}
		
		
		//======================================================================
		// children
		//======================================================================
		
		@Override
		@SuppressWarnings("unchecked")
		public T addChild(T child) {
			children.add(child);
			
			if(child.getParent() == null || !child.getParent().equals(this))
				child.setParent((T) this);
			
			return child;
		}
		@Override
		public T removeChild(T child) {
			children.remove(child);
			
			if(child.hasParent() && child.getParent().equals(this))
				child.setParent(null);
			
			return child;
		}
		
		//checks
		@Override
		public boolean hasChild(String name) {
			for(T child : children) {
				if(!child.getName().equals(name))
					continue;
				
				return true;
			}
			
			
			return false;
		}
		@Override
		public boolean hasChild(T child) {
			return children.contains(child);
		}
		
		//getter
		public T getChild(String name) {
			for(T child : children) {
				T searched = child.getBone(name);
				
				if(searched != null)
					return searched;
			}
			
			return null;
		}
		@Override
		public List<T> getChildren() {
			return children;
		}
		
		
		//======================================================================
		// parent
		//======================================================================
		
		//setter
		@SuppressWarnings("unchecked")
		@Override
		public T setParent(T parent) {
			T old = this.parent;
			
			this.parent = parent;
			
			if(parent == null) {
				if(old != null)
					old.removeChild((T) this);
				
				return null;
			}
			
			if(!parent.hasChild(name))
				parent.addChild((T) this);
				
			return parent;
		}
		
		//getter
		@Override
		public T getParent() {
			return parent;
		}
		
		@Override
		public float getParentLength() {
			if(!hasParent())
				return 0;
			
			return getParent().getLength();
		}
		
		//checks
		@Override
		public boolean hasParent() {
			return parent != null;
		}
		
		@Override
		public String toString() {
			return getName();
		}
	}
}
