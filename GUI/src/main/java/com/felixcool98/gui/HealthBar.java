package com.felixcool98.gui;

import com.kotcrab.vis.ui.widget.VisProgressBar;

public class HealthBar extends VisProgressBar {
	public HealthBar(float max, float stepSize, boolean vertical, String styleName) {
		super(0, max, stepSize, vertical, styleName);
	}
	public HealthBar(float max, float stepSize, boolean vertical, ProgressBarStyle style) {
		super(0, max, stepSize, vertical, style);
	}
	public HealthBar(float max, float stepSize, boolean vertical) {
		super(0, max, stepSize, vertical);
	}
	
	private float health;
	
	
	//======================================================================
	// setting hp
	//======================================================================
	public void setHealth(float hp) {
		health = hp;
		setValue(health);
	}
	public void addHP(float hp) {
		setHealth(getHealth() + hp);
	}
	
	
	public void setMaxHp(float maxHP) {
		setRange(0, maxHP);
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	public float getHealth() {
		return health;
	}
}
