package widgets;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.felixcool98.game.ImprovedGameState;
import com.felixcool98.widgets.html.HTMLTableBuilder;

public class HTMLTableBuilderTest extends ImprovedGameState {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("HTMLTableBuilderTest");
		
		config.setWindowSizeLimits(700, 500, 700, 500);
		
		new Lwjgl3Application(new HTMLTableBuilderTest(), config);
	}
	
	
	@Override
	public void create() {
		Table table = new HTMLTableBuilder(createText()).build();
		
		table.align(Align.bottomLeft);
		
		getStage().addActor(table);
		
		getStage().setDebugAll(true);
	}
	
	
	public static String createText() {
		String text = "text at beginning"
				+ "<p style=\"background-color: #FF0000; color: #FFFF00\">"
				+ "This is a paragraph"
				+ "</p>"
				+ "<p>"
				+ "another one<br>with line break"
				+ "</p>"
				+ "text at end";
		
		return text;
	}
}
