package com.felixcool98.gml.actions;

import com.felixcool98.gml.Scope;
import com.felixcool98.gml.Scope.ScopeType;
import com.felixcool98.gml.objects.GMLObject;

public class SetValueToConstant implements Action {
	private String name;
	private Object to;
	private ScopeType type;
	
	
	public SetValueToConstant(String name, Object to, ScopeType type) {
		this.name = name;
		this.to = to;
		this.type = type;
	}
	
	
	@Override
	public void execute(Scope global, Scope object, Scope local) {
		Scope scope = Scope.getScope(global, object, local, type, name);
		
		GMLObject set = scope.get(name);
		
		if(set == null) {
			set = new GMLObject();
			
			scope.add(name, set);
		}
		
		set.setValue(to);
	}
}
