package com.felixcool98.modding.directory;

public abstract class ModDirectoryProcessor {
	/**
	 * all objects should be created here<br>
	 * if one mod is dependent on another you can get the mod with manager.getMod(modName)
     * and add a ModObjectRegisteryListener to it
	 * 
	 * <br>
	 * 
	 * @param manager
	 * @param directory
	 */
	public abstract void process(ModDirectoryManager manager, ModDirectory directory);
}
