package com.felixcool98.widgets.imageeditor.colors;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;

/**
 * Field for displaying and selecting a single color 
 * 
 * @author felixcool98
 */
public class ColorField extends Image implements Disposable {
	private List<SelectedColorListener> listeners = new LinkedList<>();
	
	private Pixmap pixMap;
	
	
	/**
	 * Field for displaying a single color<br>
	 * use {@link ColorField#setColor(Color)} to change its color
	 */
	public ColorField() {
		super();
		
		setTouchable(Touchable.enabled);
		
		addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				for(SelectedColorListener listener : listeners) {
					listener.selectedColor(getColor());
				}
			}
		});
		
		pixMap = new Pixmap(4, 4, Format.RGBA8888);
		pixMap.setColor(Color.WHITE);
		pixMap.fill();
		
		setDrawable(new TextureRegionDrawable(new TextureRegion(new Texture(pixMap))));
	}
	
	
	/**
	 * adds a listener to the {@link ColorField}
	 * 
	 * @param listener the listener to add
	 */
	public void addListener(SelectedColorListener listener) {
		listeners.add(listener);
	}
	/**
	 * removes a listener from the {@link ColorField}
	 * 
	 * @param listener the listener to remove
	 */
	public void removeListener(SelectedColorListener listener) {
		listeners.remove(listener);
	}
	
	
	@Override
	public void dispose() {
		pixMap.dispose();
	}
	
	
	/**
	 * simple listener<br>
	 * will be called when the {@link ColorField} is clicked and will send its color to all listeners
	 * 
	 * @author felixcool98
	 */
	public static interface SelectedColorListener {
		/**
		 * called when a {@link ColorField} is clicked
		 * 
		 * @param newColor the color of the field clicked
		 */
		public void selectedColor(Color newColor);
	}
}
