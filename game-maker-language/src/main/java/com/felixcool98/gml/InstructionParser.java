package com.felixcool98.gml;

import java.util.LinkedList;
import java.util.List;

public class InstructionParser {
	private List<Instruction> instructions;
	
	private String current = "";
	private int line = 0;
	
	private boolean ifParse = false;
	private boolean forParse = false;
	
	
	public Instruction[] parse(String text) {
		return parse(text.toCharArray(), 0);
	}
	public Instruction[] parse(char[] chars, int startLine) {
		instructions = new LinkedList<>();
		current = "";
		line = startLine;
		
		for(int i = 0; i < chars.length; i++) {
			evaluate(chars[i]);
		}
		
		return instructions.toArray(new Instruction[0]);
	}
	
	private void evaluate(char c) {
		current += c;
		
		if(c == '\n' || c == '\r') {
			line ++;
		}
		
		if(ifParse) {
			if(current.endsWith(")")) {
				push(current);
				
				current = "";
				
				ifParse = false;
			}
			
			return;
		}
		if(forParse) {
			if(current.endsWith(")")) {
				push(current);
				
				current = "";
				
				forParse = false;
			}
		}
		
		if(current.contains("else")) {
			push(current);
			
			current = "";
			
			return;
		}
		
		if(!forParse && (c == '\n' || c == '\r' || c == ';')) {
			push(current);
			
			current = "";
			
			return;
		}
		
		if(current.contains("if")) {
			ifParse = true;
			
			return;
		}
		if(current.contains("for")) {
			forParse = true;
			
			return;
		}
	}
	private void push(String text) {
		Instruction instruction = new InstructionBuilder().parse(text, line-1);
		
		if(instruction.isEmpty())
			return;
		
		instructions.add(instruction);
	}
	
	
	public int getLine() {
		return line;
	}
}
