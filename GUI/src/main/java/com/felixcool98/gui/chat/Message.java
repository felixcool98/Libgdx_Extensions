package com.felixcool98.gui.chat;

import java.io.IOException;
import java.util.Date;

import com.badlogic.gdx.utils.DataInput;

public class Message {
	private Date date = new Date();
	
	
	public Message() {
		
	}
	
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	public Date getDate() {
		return date;
	}
	
	public static class NameTextMessage extends Message {
		private String name;
		private String text;
		
		
		public NameTextMessage() {
			// TODO Auto-generated constructor stub
		}
		public NameTextMessage(DataInput input) throws IOException {
			name = input.readString();
			text = input.readString();
		}
		public NameTextMessage(String name, String text) {
			this.name = name;
			this.text = text;
		}
		
		
		public String getName() {
			return name;
		}
		public String getText() {
			return text;
		}

		
		@Override
		public String toString() {
			return getDate().toString()+" "+name+": "+text;
		}
		
		@Override
		public int hashCode() {
			return (int) (name.hashCode()+getDate().getTime());
		}
	}
	
	public static class ConnectionMessage extends Message {
		private String status;
		private String name;
		
		public ConnectionMessage() {
			
		}
		public ConnectionMessage(DataInput input) throws IOException {
			name = input.readString();
			status = input.readString();
		}
		public ConnectionMessage(String name, String status) {
			this.name = name;
			this.status = status;
		}
		
		
		public String getStatus() {
			return status;
		}
		public String getName() {
			return name;
		}
		
		
		@Override
		public String toString() {
			if(name == null && status == null)
				return "";
			
			return getDate().toString()+" "+name +" "+status;
		}
		
		@Override
		public int hashCode() {
			return (int) (name.hashCode()+getDate().getTime());
		}
	}
}
