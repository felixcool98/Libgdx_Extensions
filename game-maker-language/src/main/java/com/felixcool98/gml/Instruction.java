package com.felixcool98.gml;

public class Instruction {
	private String text;
	private int line;
	private InstructionType type;
	
	
	public Instruction(String text, int line, InstructionType type) {
		this.text = text;
		this.line = line;
		this.type = type;
	}
	
	
	public String getText() {
		return text;
	}
	public int getLine() {
		return line;
	}
	public InstructionType getType() {
		return type;
	}
	
	public String getTextBehindEquals() {
		if(!getText().contains("="))
			return "";
		
		return getText().substring(text.indexOf("=")+1, text.length()).trim();
	}
	
	
	public boolean isEmpty() {
		if(getType() == InstructionType.Comment && (text.equals("//") || text.equals("///")))
			return true;
		
		return text.length() == 0;
	}
	
	
	@Override
	public String toString() {
		return getType() + " " + getLine() + " " + getText();
	}
	
	
	public static enum InstructionType {
		Comment,
		SetValue,
		Brackets,
		IF,
		ELSE,
		FOR,
		RETURN,
		FUNCTION,
		DECLARATION;
	}
}
